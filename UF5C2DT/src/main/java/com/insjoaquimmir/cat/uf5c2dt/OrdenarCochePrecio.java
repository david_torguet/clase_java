/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.insjoaquimmir.cat.uf5c2dt;

import java.util.Comparator;

/**
 *
 * @author tarda
 */
public class OrdenarCochePrecio implements Comparator<Coche>{

    @Override
    public int compare(Coche o1, Coche o2) {
        return o1.preu - o2.preu;
    }
    
}
