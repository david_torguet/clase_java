/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.insjoaquimmir.cat.uf5c2dt;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.Vector;

/**
 *
 * @author tarda
 */
public class Aplication {
    public static void main(String[] args) {        
        Scanner input = new Scanner(System.in);
        
        int num;
        int num2;
        int num3;
        int num4;
        int num5;
        Coche c1 = new Coche("Renault","megane",1234);
        Coche c2 = new Coche("Honda","civic",5000);
        Coche c3 = new Coche("Seat","ibiza",345);
        
        
        ArrayList<Coche> Coches = new ArrayList<Coche>();
        Coches.add(c3);
        Coches.add(c1);
        Coches.add(c2);
        
        Set<Coche> CochesSet = new HashSet<Coche>(Coches);
        
        Comparator<Coche> cmp_precio = new OrdenarCochePrecio();
        SortedSet<Coche> CochesSortedSet = new TreeSet<Coche>(cmp_precio);
        CochesSortedSet.addAll(Coches);
        
        SortedSet<Coche> CochesSortedSet1 = new TreeSet<Coche>(Coches);
        
        
        do{
            //num = 0;
            System.out.println("0-Crear coche\n1-Ej1\n2-Ej2\n3-Ej3\n4-Ej4\n5-Salir");
            num = input.nextInt();
            switch(num){
                case 0:
                    try{
                        System.out.println("Dame la marca de coche: ");
                        String marca = input.next();
                        System.out.println("Dame el modelo del coche: ");
                        String modelo = input.next();
                        System.out.println("Dame el precio del coche: ");
                        int precio = input.nextInt();                    
                        Coche c = new Coche(marca,modelo,precio);
                        System.out.println(c.toString());
                        //Add al array list
                        Coches.add(c);
                        //Add al Set 
                        CochesSet.add(c);
                        
                    }catch(IllegalArgumentException e){
                        System.out.println(e);
                    }
                    
                case 1:
                    System.out.println("\n1-Enumeration\n2-Iterator\n3-ListIterator");
                    num2 = input.nextInt();
                    switch(num2){
                        case 1:
                            try{
                                Enumeration cars1;
                                cars1 = Collections.enumeration(Coches);
                                while (cars1.hasMoreElements()) {
                                   System.out.println(cars1.nextElement()); 
                                }
                            }catch(IndexOutOfBoundsException e){
                                System.out.println(e);
                            }
                            break;
                            
                        case 2:                            
                            try{
                                Iterator<Coche> iterator = Coches.iterator();
                                while(iterator.hasNext()){
                                    System.out.println(iterator.next());
                                }
                            }catch(IndexOutOfBoundsException e){
                                System.out.println(e);
                            }
                            break;
                        case 3:
                            try{
                                ListIterator<Coche> it = Coches.listIterator();
                                while(it.hasNext()){
                                    it.next();
                                }
                                while(it.hasPrevious()){
                                    System.out.println(it.previous());
                                }
                            }catch(IndexOutOfBoundsException e){
                                System.out.println(e);
                            }
                            break;
                    }
                case 2:
                    System.out.println("\n1-Orden Natural\n2-Ordenar Precio\n");
                    num3 = input.nextInt();
                    switch(num3){
                        case 1:                          
                            System.out.println(CochesSortedSet1);
                            break;
                        case 2:
                            System.out.println(CochesSortedSet);                            
                            break;
                    }
                
                case 3:
                    System.out.println("\n1-Orden Natural\n2-Ordenar Precio\n3-Orden Natural Inverso\n4-Añadir coche y borrar ultimo");
                    num4 = input.nextInt();
                    
                    switch(num4){
                        case 1:                            
                            List linkedList = new LinkedList<Coche>(Coches);
                            System.out.println(linkedList);
                            break;
                        case 2:
                            List linkedListPrecio = new LinkedList<Coche>(Coches);
                            Collections.sort(linkedListPrecio, cmp_precio);
                            System.out.println(linkedListPrecio);                            
                            break;
                        case 3:
                            LinkedList linkedList2 = new LinkedList<Coche>(Coches);
                            Collections.reverse(linkedList2);                            
                            System.out.println(linkedList2);
                            break;
                        case 4:
                            LinkedList CochesLinked = new LinkedList<Coche>(Coches);
                            System.out.println("Dame la marca de coche: ");
                            String marca = input.next();
                            System.out.println("Dame el modelo del coche: ");
                            String modelo = input.next();
                            System.out.println("Dame el precio del coche: ");
                            int precio = input.nextInt();                    
                            Coche c = new Coche(marca,modelo,precio);
                            
                            CochesLinked.addFirst(c);
                            CochesLinked.removeLast();
                            System.out.println(CochesLinked);
                            break;    
                    }
                
                case 4:
                    System.out.println("\n1-Map\n2-SprtedMap");
                    num5 = input.nextInt();
                    switch(num5){
                        case 1:                            
                            Map<Integer,Coche> CocheMap = new HashMap<Integer,Coche>();
                            CocheMap.put(1, c1);
                            CocheMap.put(2, c2);
                            CocheMap.put(3, c3);
                            System.out.println(CocheMap.toString().replaceAll(",", "\n"));
                            break;
                        case 2:
                            Map<Integer,Coche> CocheCortedMap = new HashMap<Integer,Coche>();
                            //Desordenat i despres a la consola s'ordena.
                            CocheCortedMap.put(2,c2);
                            CocheCortedMap.put(1,c1);
                            CocheCortedMap.put(3,c3);
                            System.out.println(CocheCortedMap.toString().replaceAll(",", "\n"));
                            break;
                    }
            }
                
        }while(num != 5);
    }
    
}
