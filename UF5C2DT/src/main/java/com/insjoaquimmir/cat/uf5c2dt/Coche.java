/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.insjoaquimmir.cat.uf5c2dt;

import java.util.Objects;

/**
 *
 * @author tarda
 */
public class Coche implements Comparable<Coche>{
    
    //<editor-fold defaultstate="collapsed" desc="Atributs">
        private String marca;
        private String model;
        public int preu;        
    //</editor-fold>
        
        
    //<editor-fold defaultstate="collapsed" desc="Constructor">
        
    public Coche(String marca, String model, int preu) {
        this.setMarca(marca);
        this.setModel(model);
        this.setPreu(preu);
    }
    
    public Coche() {
        this("No marca","No model",0);
    }
    //</editor-fold>
    
    
    //<editor-fold defaultstate="collapsed" desc="Get-Set">
        /**
        * @return the marca
        */
        public String getMarca() {
            return marca;
        }

        /**
         * @param marca the marca to set
         */
        public void setMarca(String marca) {
            this.marca = marca;
        }

        /**
         * @return the model
         */
        public String getModel() {
            return model;
        }

        /**
         * @param model the model to set
         */
        public void setModel(String model) {
            this.model = model;
        }

        /**
         * @return the preu
         */
        public int getPreu() {
            return preu;
        }

        /**
         * @param preu the preu to set
         */
        public void setPreu(int preu) {       
            if(preu <= 0){
                throw new IllegalArgumentException(String.format("El precio %d es erroneo, no  puede ser negativo", preu));
            }else{
                this.preu = preu;    
            }
        }
    //</editor-fold>

              
    //<editor-fold defaultstate="collapsed" desc="Sobreescritura">
        
        
    @Override
     public String toString() {
         return String.format("Coche:\n---------\nMarca %s\nModel: %s\nPreu %d", this.getMarca(),this.getModel(),this.getPreu());
     }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null)
            return false;
        if (getClass() != o.getClass())
            return false;
        if (!(o instanceof Coche))
            return false;
        
        Coche that = (Coche) o;
        return Objects.equals(this.marca, that.marca)
                && Objects.equals(this.model, that.model);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 31 * hash + Objects.hashCode(this.marca);
        hash = 31 * hash + Objects.hashCode(this.model);
        return hash;
    }

    @Override
    public int compareTo(Coche o) {
        return this.marca.compareTo(o.marca);
    }
        
        
    //</editor-fold>
}
