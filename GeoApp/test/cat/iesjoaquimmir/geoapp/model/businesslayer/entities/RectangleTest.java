/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.iesjoaquimmir.geoapp.model.businesslayer.entities;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author tarda
 */
public class RectangleTest {
    
    public RectangleTest() {
    }
    //<editor-fold defaultstate="collapsed" desc="simulacio">
        private Rectangle r1, r2, r3, r4, r5;
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Cicle de Vida">
        @BeforeClass
        public static void loadTest() {
            System.out.printf("Es carrega el joc de proves %s al Test Runner.%n", ColorTest.class.getSimpleName());
        }

        @Before
        public void setUp() {
            r1 = new Rectangle(Rectangle.LADO2_VALUE,Rectangle.LADO2_VALUE,new AlphaColor(Color.MAX_VALUE, Color.MIN_VALUE, Color.MIN_VALUE),new AlphaColor(Color.MAX_VALUE, Color.MIN_VALUE, Color.MIN_VALUE));
            r2 = new Rectangle(20,250,new AlphaColor(Color.MAX_VALUE, Color.MIN_VALUE, Color.MIN_VALUE),new AlphaColor(Color.MAX_VALUE, Color.MIN_VALUE, Color.MIN_VALUE));
            r3 = new Rectangle(Rectangle.LADO2_VALUE,Rectangle.LADO2_VALUE);
            r4 = new Rectangle(5);
            r5 = new Rectangle();
            
        }

        @After
        public void tearDown() {
        }

        @AfterClass
        public static void unLoadTest() {
            System.out.printf("S'està descarregant el joc de proves %s del test-runner.%n", RectangleTest.class.getSimpleName());
        }
    //</editor-fold>
        
    //<editor-fold defaultstate="collapsed" desc="Atributs privats">
        @Test
        public void attribute_lado1_private() throws NoSuchFieldException {
            Class r = Rectangle.class;
            Assert.assertTrue("lado1 private --> Fail!!", Modifier.isPrivate(r.getDeclaredField("lado1").getModifiers()));
        }
        @Test
        public void attribute_lado2_private() throws NoSuchFieldException {
            Class r = Rectangle.class;
            Assert.assertTrue("lado2 private --> Fail!!", Modifier.isPrivate(r.getDeclaredField("lado2").getModifiers()));
        }
        
        @Test
        public void attribute_static_LADO2_VALUE() {
            Assert.assertEquals("Rectangle.LADO2_VALUE --> Fail!!", 1.0, Rectangle.LADO2_VALUE,0);
        }

        @Test
        public void attribute_static_MIN_VALUE_final() throws NoSuchFieldException {
            Class r = Rectangle.class;
            Field f = r.getField("LADO2_VALUE");
            Assert.assertTrue("Rectangle.LADO2_VALUE  is final --> Fails!!", Modifier.isFinal(f.getModifiers()));
        }
    //</editor-fold>
        
    //<editor-fold defaultstate="collapsed" desc="Constructores">
        @Test(expected = IllegalArgumentException.class)
        public void ctor_2params_error() {
           Rectangle r = new Rectangle(-8,-6);           
        }
        @Test(expected = IllegalArgumentException.class)
        public void ctor_1params_lado1_error() {
           Rectangle r = new Rectangle(-8);           
        }
        @Test(expected = IllegalArgumentException.class)
        public void ctor_params_rectangles_error() {
           Rectangle r = new Rectangle(8,6,new AlphaColor(256,200,200),new AlphaColor(256,200,200));           
        }
        
    //</editor-fold>
        
    //<editor-fold defaultstate="collapsed" desc="Gett-Sett">
        @Test
        public void rectangle_setLado1_25() {
           r1.setLado1(25);
           Assert.assertEquals("c1.setRed(122) --> Fails!!", 25, r1.getLado1(),0);
        }
        @Test
        public void rectangle_setLado2_25() {
           r1.setLado2(25);
           Assert.assertEquals("c1.setRed(122) --> Fails!!", 25, r1.getLado2(),0);
        }
        
        @Test(expected = IllegalArgumentException.class)
        public void rectangle_setLado1_0() {
           r1.setLado1(0);
        }
        @Test(expected = IllegalArgumentException.class)
        public void rectangle_setLado2_0() {
           r1.setLado2(0);
        }
        
        @Test(expected = IllegalArgumentException.class)
        public void rectangle_setLado1_menor_0() {
           r1.setLado1(-1);
        }
        @Test(expected = IllegalArgumentException.class)
        public void rectangle_setLado2_menor_0() {
           r1.setLado2(-1);
        }
        
    //</editor-fold>
        
    //<editor-fold defaultstate="collapsed" desc="Metdode Area y Perimetre">
        @Test
        public void rectangle_calc_area() {
           r1.setLado1(5);
           r1.setLado2(5);
           Assert.assertEquals("c1.setRed(122) --> Fails!!", 25, r1.getArea(),0);
        }
        @Test
        public void rectangle_calc_per() {
           r1.setLado1(5);
           r1.setLado2(5);
           Assert.assertEquals("c1.setRed(122) --> Fails!!", 20, r1.getPerimeter(),0);
        }
    //</editor-fold>
        
    //<editor-fold defaultstate="collapsed" desc="toString">
        @Test
        public void rectangle_toString() {
            Rectangle r = new Rectangle(5,5,new AlphaColor(250,200,200),new AlphaColor(250,200,200));
            String lado1=String.format("%.2f",r.getLado1());
            String lado2=String.format("%.2f",r.getLado2());
            String area=String.format("%.2f",r.getArea());
            String perimetre=String.format("%.2f",r.getPerimeter());
            Assert.assertTrue("rectangle.toString() --> No muestra el parametre lado1 del rectangle", r.toString().contains(String.valueOf(lado1)));
            Assert.assertTrue("rectangle.toString() --> No muestra el parametre lado2 del rectangle", r.toString().contains(String.valueOf(lado2)));
            Assert.assertTrue("rectangle.toString() --> No muestra el parametre Area del rectangle", r.toString().contains(String.valueOf(area)));
            Assert.assertTrue("rectangle.toString() --> No muestra el parametre Perimetre del rectangle", r.toString().contains(String.valueOf(perimetre)));
            Assert.assertTrue("rectangle.toString() --> No muestra el parametre Background del rectangle", r.toString().contains(String.valueOf(r.getBackgroundColor().toHexString())));
            Assert.assertTrue("rectangle.toString() --> No muestra el parametre Foreground del rectangle", r.toString().contains(String.valueOf(r.getForegroundColor().toHexString())));
        }
    //</editor-fold>
        
        
    
}
