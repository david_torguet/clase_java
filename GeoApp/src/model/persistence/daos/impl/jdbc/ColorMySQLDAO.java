/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.persistence.daos.impl.jdbc;

import cat.iesjoaquimmir.geoapp.model.businesslayer.entities.AlphaColor;
import cat.iesjoaquimmir.geoapp.model.businesslayer.entities.ColorDao;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.persistence.utilities.JDBCUtils;

    /**
 *
 * @author tarda
 */
public class ColorMySQLDAO implements ColorDao{
    
    /**
     *
     * @param id
     * @return
     * @throws PersistenceException
     */
    @Override
    public AlphaColor getColorById(long id) throws PersistenceException {
        AlphaColor color = null;
        try(Connection conn = DriverManager.getConnection
                ("jdbc:mysql://192.168.0.56/torguetd_geoapp", "torguetd", "torguetd");
            PreparedStatement sql = conn.prepareStatement
                ("SELECT id, red, green, blue, alpha FROM colors where id = ?");){
            sql.setLong(1, id);
            try(ResultSet reader = sql.executeQuery();){
                if (reader.next()){
                    color = JDBCUtils.getAlphaColor(reader);
                }
            }
        } catch (SQLException e){
            throw new PersistenceException(e.getErrorCode());
        }
        return color; }

        @Override
        public List<AlphaColor> getColors() throws PersistenceException {
            List<AlphaColor> colors = new ArrayList<>();
            try(Connection conn = DriverManager.getConnection
                    ("jdbc:mysql://192.168.0.56/torguetd_geoapp", "torguetd", "torguetd");
                PreparedStatement sql = conn.prepareStatement
                    ("SELECT id, red, green, blue, alpha FROM colors");
                //CallableStatement sql = conn.prepareCall("CALL getColors()");
                ResultSet reader = sql.executeQuery();){ 
                while (reader.next()){ // preguntem a reader si hi han resultats a llegir
                    // recollir dades i crear objectes AlphaColor
                    colors.add(JDBCUtils.getAlphaColor(reader));
                }
                sql.close();
                conn.close();
                reader.close();
            } catch (SQLException ex) {
                throw new PersistenceException(ex.getErrorCode());
            }
            return colors;
        }

       @Override
    public void saveColor(AlphaColor color) {
        try(Connection conn = DriverManager.getConnection
                ("jdbc:mysql://192.168.0.56/torguetd_geoapp", "torguetd", "torguetd");
                PreparedStatement sql = conn.prepareStatement
                    ("INSERT into colors  (red, green, blue, alpha)values (?,?,?,?)");){
            //CallableStatement sql = conn.prepareCall("CALL saveColor(?,?,?,?)");){
            sql.setInt(1, color.getRed());
            sql.setInt(2, color.getGreen());
            sql.setInt(3, color.getBlue());
            sql.setDouble(4, color.getAlpha());
            sql.execute();
            sql.close();
            conn.close();            
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
            
        
    }
  }

    

