/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.iesjoaquimmir.geoapp.model.businesslayer.entities;

import java.util.List;
import model.persistence.daos.impl.jdbc.PersistenceException;

/**
 *
 * @author tarda
 */
public interface ColorDao {
        
        public abstract AlphaColor getColorById(long id) throws PersistenceException;
        public abstract List<AlphaColor> getColors() throws PersistenceException;
        public abstract void saveColor(AlphaColor color);
    
}
