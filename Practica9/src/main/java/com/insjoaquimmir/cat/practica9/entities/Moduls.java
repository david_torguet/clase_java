/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.insjoaquimmir.cat.practica9.entities;

/**
 *
 * @author tarda
 */
public class Moduls {
    
    
    
    //<editor-fold defaultstate="collapsed" desc="Atributs">
        private String nom;
        private String descripcio;
        private int hores;
    //</editor-fold>
        
    //<editor-fold defaultstate="collapsed" desc="Metodes">
        //<editor-fold defaultstate="collapsed" desc="Getters Setters">
            /**
            * @return the nom
            */
           public String getNom() {
               return nom;
           }

           /**
            * @param nom the nom to set
            */
           public void setNom(String nom) {
               this.nom = nom;
           }

           /**
            * @return the descripcio
            */
           public String getDescripcio() {
               return descripcio;
           }

           /**
            * @param descripcio the descripcio to set
            */
           public void setDescripcio(String descripcio) {
               this.descripcio = descripcio;
           }

           /**
            * @return the hores
            */
           public int getHores() {
               return hores;
           }

           /**
            * @param hores the hores to set
            */
           public void setHores(int hores) {
               this.hores = hores;
           }
        //</editor-fold>
           
    /**
     * Constructor con todos los parametros.
     * @param nom del Modul.
     * @param desc del Modul.
     * @param hores del Modul.
     */
    public Moduls(String nom,String desc, int hores){
            this.setNom(nom);
            this.setDescripcio(desc);
            this.setHores(hores);
        }
        
    //</editor-fold>

    
}
