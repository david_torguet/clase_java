package com.insjoaquimmir.cat.practica9.views;

import com.insjoaquimmir.cat.practica9.entities.Alumne;
import com.insjoaquimmir.cat.practica9.entities.Domicili;
import com.insjoaquimmir.cat.practica9.entities.Moduls;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Menu para crear Alumnos con Domicilios i poder asignarles Modulos a los Alumnos.
 * @author tarda
 */
public class Aplicattion {

    /**
     *
     * @param args
     */
    public static void main(String[] args) {
        Domicili d1= new Domicili("Calle agua", "45", "6-4","08800");
        Domicili d2= new Domicili("Calle agua", "4", "6-4","08800");

        Alumne al1 = new Alumne("David","Torguet","Martos","15478796J","938935874",22, d2);
        Alumne al2 = new Alumne("Albert","Berna","Fabra","15478796J","938935874",22, d1);
        
        Moduls mod1 = new Moduls("M03","Programacio orientada a objectes",122);
        Moduls mod2 = new Moduls("M04","Diseny web",800);
        Moduls mod3 = new Moduls("M06","Javascript",56);
        
        ArrayList<Alumne> alumnes = new ArrayList<Alumne>();
        alumnes.add(al1);
        alumnes.add(al2);
        
        ArrayList<Moduls> moduls = new ArrayList<Moduls>();
        moduls.add(mod1);
        moduls.add(mod2);
        moduls.add(mod3);

        al1.getModuls().add(mod1);
        alumnes.get(1).getModuls().add(moduls.get(1));
        System.out.println(alumnes.get(1).getNom_Alumne());
        
        Scanner input = new Scanner(System.in);
        int num = 0;
        do{
            System.out.println("Dime de que dispones: \n1-Nombre-DNI-Edat\n2-Nombre-Edat\n3-Nada\n4-Salir");
            num = input.nextInt();
            
        }while(num != 4);
        
    }
}
