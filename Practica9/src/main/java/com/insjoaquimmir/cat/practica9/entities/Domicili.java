package com.insjoaquimmir.cat.practica9.entities;

/**
 *
 * @author tarda
 */
public class Domicili {
    //<editor-fold defaultstate="collapsed" desc="Atributs">
        private String carrrer;
        private String numero;
        private String pis;
        private String codi_postal;
        private String poblacio;
        private String provincia;
    
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Metodes">
        //<editor-fold defaultstate="collapsed" desc="Getters Setters">
            /**
            * @return the carrrer
            */
           public String getCarrrer() {
               return carrrer;
           }

           /**
            * @param carrrer the carrrer to set
            */
           public void setCarrrer(String carrrer) {
               this.carrrer = carrrer;
           }

           /**
            * @return the numero
            */
           public String getNumero() {
               return numero;
           }

           /**
            * @param numero the numero to set
            */
           public void setNumero(String numero) {
               this.numero = numero;
           }

           /**
            * @return the pis
            */
           public String getPis() {
               return pis;
           }

           /**
            * @param pis the pis to set
            */
           public void setPis(String pis) {
               this.pis = pis;
           }

           /**
            * @return the codi_postal
            */
           public String getCodi_postal() {
               return codi_postal;
           }

           /**
            * @param codi_postal the codi_postal to set
            */
           public void setCodi_postal(String codi_postal) {
               this.codi_postal = codi_postal;
           }

           /**
            * @return the poblacio
            */
           public String getPoblacio() {
               return poblacio;
           }

           /**
            * @param poblacio the poblacio to set
            */
           public void setPoblacio(String poblacio) {
               this.poblacio = poblacio;
           }

           /**
            * @return the provincia
            */
           public String getProvincia() {
               return provincia;
           }

           /**
            * @param provincia the provincia to set
            */
           public void setProvincia(String provincia) {
               this.provincia = provincia;
           }
        //</editor-fold>

    /**
     * Constructor amb tots els parametres de la clase Domicili.
     * @param carrer del Domicili
     * @param numero del Domicili
     * @param pis del Domicili
     * @param codi_postal del Domicili
     * @param poblacio del Domicili 
     * @param provincia del Domicili
     */
        public Domicili(String carrer,String numero,String pis,String codi_postal,String poblacio, String provincia){
            this.setCarrrer(carrrer);
            this.setNumero(numero);
            this.setPis(pis);
            this.setCodi_postal(codi_postal);
            this.setPoblacio(poblacio);
            this.setProvincia(provincia);
        }
        
    /**
     *Constructor amb quatre parametres, crida al constructor amb tots els parametres amb poblacio i provincia per defecte "Default".
     * @param carrer
     * @param numero
     * @param pis
     * @param codi_postal
     */
    public Domicili(String carrer,String numero,String pis,String codi_postal){
            this(carrer,numero,pis,codi_postal,"Default","Default");
        }
        
    /**
     * Constructor buit que crea un objecte Domicili per defecte a Default.
     */
    public Domicili(){
            this("Default carrer","Default numero","Default pis","Default codi postal");
        }
        
    /**
     *Metode que retorna un String amb els parametres de l'objecte.
     * @return
     */
    public String getDomicili(){
            return String.format("%s %s, %s ,%s, %s (%s)",this.getCarrrer(),this.getNumero(),this.getPis(),this.getCodi_postal(),this.getPoblacio(),this.getProvincia());
        }
        
        
        
    //</editor-fold>

    
    
    
    
    
    
}
