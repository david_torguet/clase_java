package com.insjoaquimmir.cat.practica9.entities;

import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author tarda
 */
public class Alumne {
    //<editor-fold defaultstate="collapsed" desc="Atributs">
        private String nom;
        private String primer_cognom;
        private String segon_cognom;
        private String dni;
        private String telefon;
        private int edat;
        private Domicili domicili;
        private ArrayList<String> telefons =new ArrayList<>();
        private ArrayList<Moduls> moduls;
        
    /**
     * DNI per defecte
     */
    public static final String  DEFAULT_DNI = "00000000A";

    /**
     * Telefon per defecte
     */
    public static final String  DEFAULT_TEL = "123456789";

    /**
     * Edat per defecte
     */
    public static final int  DEFAULT_EDAT = 0;

    /**
     * Domicili per defecte creat sense variables
     */
    public static final Domicili  DEFAULT_DOM = new Domicili();

        
    //</editor-fold>
        
    //<editor-fold defaultstate="collapsed" desc="Metodes">
        //<editor-fold defaultstate="collapsed" desc="Getters Setters">
            /**
            * @return the nom
            */
           public String getNom() {
               return nom;
           }

           /**
            * @param nom the nom to set
            */
           public void setNom(String nom) {
               this.nom = nom;
           }

           /**
            * @return the primer_cognom
            */
           public String getPrimer_cognom() {
               return primer_cognom;
           }

           /**
            * @param primer_cognom the primer_cognom to set
            */
           public void setPrimer_cognom(String primer_cognom) {
               this.primer_cognom = primer_cognom;
           }

           /**
            * @return the segon_cognom
            */
           public String getSegon_cognom() {
               return segon_cognom;
           }

           /**
            * @param segon_cognom the segon_cognom to set
            */
           public void setSegon_cognom(String segon_cognom) {
               this.segon_cognom = segon_cognom;
           }

           /**
            * @return the dni
            */
           public String getDni() {
               return dni;
           }

           /**
            * @param dni the dni to set
            */
           public void setDni(String dni) {
               this.dni = dni;
           }

           /**
            * @return the telefon
            */
           public ArrayList<String> getTelefons() {
               return telefons;
           }

           /**
            * @param telefon the telefon to set
            */
           public void setTelefons(String telefon) {
               this.telefons.add(telefon);
           }

           /**
            * @return the edat
            */
           public int getEdat() {
               return edat;
           }

           /**
            * @param edat the edat to set
            */
           public void setEdat(int edat) {
               this.edat = edat;
           }

           /**
            * @return the domicili
            */
           public Domicili getDomicili() {
               return domicili;
           }

           /**
            * @param domicili the domicili to set
            */
           public void setDomicili(Domicili domicili) {
               this.domicili = domicili;
           }
           /**
            * @return the moduls
            */
           public ArrayList<Moduls> getModuls() {
               return moduls;
           }

           /**
            * @param moduls the moduls to set
            */
           public void setModuls(ArrayList<Moduls> moduls) {
               this.moduls = moduls;
           }
           
        //</editor-fold>
           
    /**
     * Constructor amb tots els parametres. Els moduls es un Array creat inicialment buit per a despres insertar-li els moduls seleccionats al menu Application
     * @param nom de Alumne
     * @param primer_c de Alumne
     * @param segon_c de Alumne
     * @param dni de Alumne
     * @param telefon de Alumne
     * @param edat de Alumne
     * @param dom de Alumne
     */
    public Alumne(String nom,String primer_c, String segon_c,String dni,String telefon,int edat, Domicili dom){
            this.setNom(nom);
            this.setPrimer_cognom(primer_c);
            this.setSegon_cognom(segon_c);
            this.setDni(dni);
            this.setTelefons(telefon);
            this.setEdat(edat);
            this.setDomicili(dom);
            this.setModuls(new ArrayList<Moduls>());
        }
        
    /**
     * Constructor amb tres parametres que crida al constructor amb tots els parametres, el dni,telefon,edat i domicili per defecte.
     * @param nom
     * @param primer_c
     * @param segon_c
     */
    public Alumne(String nom,String primer_c,String segon_c){
            this(nom,primer_c,segon_c,DEFAULT_DNI,DEFAULT_TEL,DEFAULT_EDAT,DEFAULT_DOM);
        }
        
    /**
     * Metode que printa el Nom, primer cognom i segon cognom de l'objecte Alumne creat.
     * @return El nom de l'alumne en format String
     */
    public String getNom_Alumne(){
            return String.format("%s,%s,%s",this.getPrimer_cognom(),this.getSegon_cognom(),this.getNom());
        }

    /**
     * Metode que retorna un String en funció de si es major de edat o no.
     * @return String
     */
    public String getMajorEdat(){
            return String.format(getEdat()>18 ? "Es major de edat" : "Es menor de edat");
        }
            
    
        
        
        
        
        
    //</editor-fold>

    

    

    
}