

package cat.insjoaquimmir.davidt.biblioteca.articles.publicacio;

import cat.insjoaquimmir.davidt.biblioteca.articles.Categoria;
import cat.insjoaquimmir.davidt.biblioteca.articles.Promotor;
import java.util.Objects;


public class Revista extends AbstractPublicacio  {
    //<editor-fold defaultstate="collapsed" desc="Atributs">
    private String ISSN;
    
//</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Metodes">
        //<editor-fold defaultstate="collapsed" desc="Getters/Setters">
            /**
            * @return the ISSN
            */
           public String getISSN() {
               return ISSN;
           }

           /**
            * @param ISSN the ISSN to set
            */
           public void setISSN(String ISSN) {
               this.ISSN = ISSN;
           }
        //</editor-fold>
           
        public Revista(String titol,String autor,Categoria categoria,Promotor promotor, int num_pag, String algo){
            super(titol,autor,categoria,promotor,num_pag);
            this.setISSN(algo);
        }
        public Revista(String titol,String autor,Categoria categoria, int num_pag, String algo){
            super(titol,autor,categoria,new Promotor(),num_pag);
            this.setISSN(algo);
        }
        
        
    //</editor-fold>

    @Override
    public String toString() {
        return String.format("ISSN: %s , %s ", this.getISSN(),super.toString());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null) 
            return false;
        if (getClass() != o.getClass()) 
            return false;
        if (!(o instanceof Revista))
            return false;

        Revista that = (Revista) o;
        return Objects.equals(this.ISSN, that.ISSN);
            
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 97 * hash + Objects.hashCode(this.ISSN);
        return hash;
    }
}
