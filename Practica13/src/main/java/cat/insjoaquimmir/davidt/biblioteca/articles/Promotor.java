/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.insjoaquimmir.davidt.biblioteca.articles;

public class Promotor {
//<editor-fold defaultstate="collapsed" desc="Atributs">
    private String nom;
    private String pais;
    private String telefon;
    private String email;
    private String web;
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc="GET SET">
    /**
     * @return the nom
     */
    public String getNom() {
        return nom;
    }

    /**
     * @param nom the nom to set
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     * @return the pais
     */
    public String getPais() {
        return pais;
    }

    /**
     * @param pais the pais to set
     */
    public void setPais(String pais) {
        this.pais = pais;
    }

    /**
     * @return the telefon
     */
    public String getTelefon() {
        return telefon;
    }

    /**
     * @param telefon the telefon to set
     */
    public void setTelefon(String telefon) {
        this.telefon = telefon;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the web
     */
    public String getWeb() {
        return web;
    }

    /**
     * @param web the web to set
     */
    public void setWeb(String web) {
        this.web = web;
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="Constructors">
    public Promotor(String nom, String pais, String telefon, String email, String web) {
        this.setNom(nom);
        this.setPais(pais);
        this.setTelefon(telefon);
        this.setEmail(email);
        this.setWeb(web);
    }
    
    public Promotor(String nom, String pais, String telefon, String email) {
        this(nom,pais,telefon,email,"No web");
    }
    public Promotor() {
        this("No","No","No","No");
    }
//</editor-fold>

    @Override
    public String toString() {
        return String.format("Nom: %s \nPais: %s \nTelefon: %s \nEmail: %s \nWeb: %s",this.getNom(),this.getPais(),this.getTelefon(),this.getEmail(),this.getWeb());
    }

    

    
    
    
}
