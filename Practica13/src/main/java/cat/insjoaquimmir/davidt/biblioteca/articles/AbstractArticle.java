

package cat.insjoaquimmir.davidt.biblioteca.articles;


public abstract class AbstractArticle {
    //<editor-fold defaultstate="collapsed" desc="Atributs">
    private String titol;
    private String autor;
    private Categoria cat;
    private Promotor prom;
//</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Metodes">
    //<editor-fold defaultstate="collapsed" desc="Getters/setters">
    private String getTitol() {
        return titol;
    }
    private void setTitol(String titol) {
        this.titol = titol;
    }
    private String getAutor() {
        return autor;
    }
    private void setAutor(String autor) {
        this.autor = autor;
    }

    public Categoria getCat() {
        return cat;
    }


    public void setCat(Categoria cat) {
        this.cat = cat;
    }
    
    public Promotor getProm() {
        return prom;
    }

    
    public void setProm(Promotor prom) {
        this.prom = prom;
    }

//</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Constructor">
        public AbstractArticle(String tit,String aut,Categoria cat,Promotor pr){            
            this.setTitol(tit);
            this.setAutor(aut);
            this.setCat(cat);
            this.setProm(pr);
        }
    //</editor-fold>
        
    @Override
    public String toString() {
        return String.format("Titol: %s \n Autor: %s \n Categoria: %s \nPromotor:\n----------\n %s ", this.getTitol(),this.getAutor(),this.getCat().getHola(),this.getProm());
    }
        
      
      
//</editor-fold>

    

}

