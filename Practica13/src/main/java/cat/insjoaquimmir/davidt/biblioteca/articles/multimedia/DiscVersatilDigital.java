

package cat.insjoaquimmir.davidt.biblioteca.articles.multimedia;

import cat.insjoaquimmir.davidt.biblioteca.articles.Categoria;
import cat.insjoaquimmir.davidt.biblioteca.articles.Promotor;
import cat.insjoaquimmir.davidt.biblioteca.articles.publicacio.Llibre;
import java.util.Objects;


public class DiscVersatilDigital extends AbstractMultimedia {
    //<editor-fold defaultstate="collapsed" desc="Atributs">
    private String ISAN;
//</editor-fold>

    public String getISAN() {
        return ISAN;
    }

   
    public void setISAN(String ISAN) {
        this.ISAN = ISAN;
    }    

    public DiscVersatilDigital(String tit, String aut,Categoria categoria,Promotor promotor,String durada,String algo) {
        super(tit, aut,categoria,promotor,durada);
        this.setISAN(algo);
        
    }
    public DiscVersatilDigital(String tit, String aut,Categoria categoria,String durada,String algo) {
        super(tit, aut,categoria,new Promotor(),durada);
        this.setISAN(algo);
        
    }

    @Override
    public String toString() {
        return String.format("ISAN: %s \n %s ", this.getISAN(),super.toString());
    }

    
    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null) 
            return false;
        if (getClass() != o.getClass()) 
            return false;
        if (!(o instanceof DiscVersatilDigital))
            return false;

        DiscVersatilDigital that = (DiscVersatilDigital) o;
        return Objects.equals(this.ISAN, that.ISAN);
            
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 83 * hash + Objects.hashCode(this.ISAN);
        return hash;
    }


}
