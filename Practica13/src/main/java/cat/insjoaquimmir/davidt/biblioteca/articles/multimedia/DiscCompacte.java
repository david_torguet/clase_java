

package cat.insjoaquimmir.davidt.biblioteca.articles.multimedia;

import cat.insjoaquimmir.davidt.biblioteca.articles.Categoria;
import cat.insjoaquimmir.davidt.biblioteca.articles.Promotor;
import cat.insjoaquimmir.davidt.biblioteca.articles.publicacio.Llibre;
import java.util.Objects;


public class DiscCompacte extends AbstractMultimedia {
    //<editor-fold defaultstate="collapsed" desc="Atributs">
    private String ISMN;

    
//</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Metodes">
    //<editor-fold defaultstate="collapsed" desc="Getters/Setters">
    public String getISMN() {
        return ISMN;
    }
    public void setISMN(String ISMN) {
        this.ISMN = ISMN;
    }

//</editor-fold>
    public DiscCompacte(String tit, String aut,Categoria categoria,Promotor promotor,String durada,String algo) {
        super(tit, aut,categoria,promotor,durada);
        this.setISMN(algo);
    }
    public DiscCompacte(String tit, String aut,Categoria categoria,String durada,String algo) {
        super(tit, aut,categoria,new Promotor(),durada);
        this.setISMN(algo);
    }
    
//</editor-fold>

    @Override
    public String toString() {
        return String.format("ISMN: %s \n %s", this.getISMN(),super.toString());
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null) 
            return false;
        if (getClass() != o.getClass()) 
            return false;
        if (!(o instanceof DiscCompacte))
            return false;

        DiscCompacte that = (DiscCompacte) o;
        return Objects.equals(this.ISMN, that.ISMN);
            
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 31 * hash + Objects.hashCode(this.ISMN);
        return hash;
    }

}
