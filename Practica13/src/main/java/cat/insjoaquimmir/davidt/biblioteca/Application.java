

package cat.insjoaquimmir.davidt.biblioteca;

import cat.insjoaquimmir.davidt.biblioteca.articles.AbstractArticle;
import cat.insjoaquimmir.davidt.biblioteca.usuaris.UserTipus;
import cat.insjoaquimmir.davidt.biblioteca.usuaris.UsuariMenorEdat;
import cat.insjoaquimmir.davidt.biblioteca.usuaris.UsuariAdult;
import cat.insjoaquimmir.davidt.biblioteca.articles.Categoria;
import cat.insjoaquimmir.davidt.biblioteca.articles.Promotor;
import cat.insjoaquimmir.davidt.biblioteca.articles.multimedia.DiscCompacte;
import cat.insjoaquimmir.davidt.biblioteca.articles.multimedia.DiscVersatilDigital;
import cat.insjoaquimmir.davidt.biblioteca.articles.publicacio.Llibre;
import cat.insjoaquimmir.davidt.biblioteca.articles.publicacio.Revista;
import cat.insjoaquimmir.davidt.biblioteca.usuaris.AbstractUsuari;
import java.util.ArrayList;
import java.util.Scanner;


public class Application {
    public static void main(String[] args) {
 
    //Creacio Promotor
    Promotor p1 = new Promotor("David","Spain","123456789","mail@mail.com");
    //Creacio Articles
    DiscCompacte a0 = new DiscCompacte("Mis grandes éxitos", "Luis Miguel", Categoria.A, "65", "04900009");
    DiscVersatilDigital a1 = new DiscVersatilDigital("Bamby", "Walt Disney", Categoria.I, "89", "90843089");
    DiscVersatilDigital a2 = new DiscVersatilDigital("Crepusculo", "Warner", Categoria.J, "129", "480980983");
    Llibre a3 = new Llibre("Ulises", "James Joyce", Categoria.J, 456, "940943322");
    Llibre a4 = new Llibre("Tina Super Bruixa", "Enid Blyton", Categoria.J, 460, "342090233");
    Llibre a7 = new Llibre("Tina Super Bruixa", "Enid Blyton", Categoria.J,p1, 460, "342090234");
    Revista a5 = new Revista("Patufet", "Josep M. Folch i Torres", Categoria.I,p1, 87, "80002122");

    //Creacio Usuaris
    UsuariAdult usuariPremium = new UsuariAdult("Pepe", "Garcia", "Coronado","498878974K", UserTipus.P);
    UsuariAdult usuariNormal = new UsuariAdult("Paco", "Lopez", "Picasso","498878214M", UserTipus.N);
    UsuariMenorEdat usuariMenorPremium = new UsuariMenorEdat("Pedro", "Garcia", "Saez",usuariPremium);
    UsuariMenorEdat usuariMenorNormal = new UsuariMenorEdat("Paulo", "Lopez", "Moliere",usuariNormal);
    
    //Añadir Articles a Usuaris
    usuariPremium.agafaArticle(a0);
    usuariNormal.agafaArticle(a1);
    usuariMenorPremium.agafaArticle(a3);
    usuariMenorNormal.agafaArticle(a4);
    usuariMenorNormal.agafaArticle(a5);
    
    //Array de Usuaris
    ArrayList<AbstractUsuari> array_users = new ArrayList<AbstractUsuari>();
    array_users.add(usuariPremium);
    array_users.add(usuariNormal);
    array_users.add(usuariMenorPremium);
    array_users.add(usuariMenorNormal);
    
    //Array de Articles
    ArrayList<AbstractArticle> array_articles = new ArrayList<AbstractArticle>();
    array_articles.add(a0);
    array_articles.add(a1);
    array_articles.add(a2);
    array_articles.add(a3);
    array_articles.add(a4);
    array_articles.add(a7);
    array_articles.add(a5);
    
    
    //for(int x = 0;x < array_users.size();x++){
        
    //    System.out.println(array_users.get(x).toString());
    //}
      
    Scanner input = new Scanner(System.in);
    int num = 0;
    boolean seguir = true;
    
        do{
            System.out.println("\n-----------------------------------------------------------------------------");
            System.out.println("Dame un numero: \n1-Mostrar Array Articles\n2-Mostrar Array Usuaris\n3-Crear Usuari Adult\n4-Mostrar Articles de Usuaris\n5-Salir");
            num = input.nextInt();
            
            switch(num){
                    case 1:
                        for(int x = 0;x < array_articles.size();x++){
                            System.out.println(array_articles.get(x).toString());
                            System.out.println("-----------------------------------------");
                        }
                        break;
                    case 2:
                        for(int x = 0;x < array_users.size();x++){
                            System.out.println(array_users.get(x).toString());
                            System.out.println("-----------------------------------------");
                        }
                        break;
                    case 3:
                        System.out.println("Dime el nombre del Usuario:\n");
                        String nom = input.next();
                        System.out.println("Dime el Primer Apellido del Usuario:\n");
                        String apellido1 = input.next();
                        System.out.println("Dime el Segundo Apellido del Usuario:\n");
                        String apellido2 = input.next();
                        System.out.println("Dime el DNI del Usuario:\n");
                        String dni = input.next();
                        System.out.println("Dime la categoria del Usuario(Premiun-P o Normal-N)");
                        String userTipus = input.next();
                        if(userTipus.equals("P")){
                            UsuariAdult u1 = new UsuariAdult(nom,apellido1,apellido2,dni,UserTipus.P);                                                        
                            if(array_users.contains(u1)){
                                System.out.println("El Usuario ya existe.");
                            }else{
                                System.out.println("Nou usuari: \n");
                                System.out.println(u1.toString());
                                array_users.add(u1);    
                            }
                            
                        }else if(userTipus.equals("N")){
                            UsuariAdult u1 = new UsuariAdult(nom,apellido1,apellido2,dni,UserTipus.N);
                            
                            if(array_users.contains(u1)){
                                System.out.println("El Usuario ya existe.");
                            }else{
                                System.out.println("Nou usuari: \n");
                                System.out.println(u1.toString());
                                array_users.add(u1);    
                            }
                        }                                                
                        break;                        
                    case 4:
                        for(int x = 0;x < array_users.size();x++){   
                            System.out.println(array_users.get(x).toString());
                            System.out.printf("Array Articles: %d \n",array_users.get(x).sizeArrayArticles());
                            for(int i = 0;i < array_users.get(x).sizeArrayArticles();i++){                                
                                System.out.println(array_users.get(x).getArrayArt().get(i).toString());
                                System.out.println("----------------");
                            }
                            System.out.println("-----------------------------------------");

                        }
                    case 5:
                        seguir = false;
                    default:
                        seguir = false;
            }            
        }while(seguir);
        
    
    
    }
}
