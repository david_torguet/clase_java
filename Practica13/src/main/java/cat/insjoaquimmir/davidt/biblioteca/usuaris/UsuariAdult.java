/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.insjoaquimmir.davidt.biblioteca.usuaris;

import cat.insjoaquimmir.davidt.biblioteca.articles.AbstractArticle;
import java.util.ArrayList;
import java.util.Objects;


public class UsuariAdult extends AbstractUsuari{
    
    private String DNI;
    private UserTipus tipus;
    
    public UsuariAdult(String Nom, String cognom1, String cognom2,String dni,UserTipus tip) {
        super(Nom, cognom1, cognom2, new ArrayList<AbstractArticle>());
        this.setDNI(dni);
        this.setTipus(tip);
    }
//<editor-fold defaultstate="collapsed" desc="GET SET">
    /**
     * @return the DNI
     */
    public String getDNI() {
        return DNI;
    }

    /**
     * @param DNI the DNI to set
     */
    public void setDNI(String DNI) {
        this.DNI = DNI;
    }
    /**
     * @return the tipus
     */
    public UserTipus getTipus() {
        return tipus;
    }

    /**
     * @param tipus the tipus to set
     */
    public void setTipus(UserTipus tipus) {
        this.tipus = tipus;
    }
//</editor-fold>

    /**
     *
     * @return
     */
    @Override
    protected boolean isMenor(){
        return false;
    }
    
    @Override
    protected int cantidad(){
        return this.getTipus().getCuantitat();
    }

    
    
    
    
    public String toString() {
        return String.format("Usuari Adult \n Dni: %s \n %s", this.getDNI(),super.toString());
    }
    
    
    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null) 
            return false;
        if (getClass() != o.getClass()) 
            return false;
        if (!(o instanceof UsuariAdult))
            return false;

        UsuariAdult that = (UsuariAdult) o;
        return Objects.equals(this.DNI, that.DNI);
            
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 23 * hash + Objects.hashCode(this.DNI);
        return hash;
    }

    
}
