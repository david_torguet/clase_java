

package cat.insjoaquimmir.davidt.biblioteca.articles.publicacio;

import cat.insjoaquimmir.davidt.biblioteca.articles.AbstractArticle;
import cat.insjoaquimmir.davidt.biblioteca.articles.Categoria;
import cat.insjoaquimmir.davidt.biblioteca.articles.Promotor;


public abstract class AbstractPublicacio extends AbstractArticle {
    //<editor-fold defaultstate="collapsed" desc="Atributs">
        private int NumPg;
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="GET SET">
        /**
        * @return the NumPg
        */
       public int getNumPg() {
           return NumPg;
       }

       /**
        * @param NumPg the NumPg to set
        */
       public void setNumPg(int NumPg) {
           this.NumPg = NumPg;
       }
   //</editor-fold>

    
    public AbstractPublicacio(String tit, String aut,Categoria categoria,Promotor promotor, int num_pag) {
        super(tit,aut,categoria,promotor);
        this.setNumPg(num_pag);
    }

    @Override
    public String toString() {
        return String.format("Num Pag: %d \n %s", this.getNumPg(),super.toString());
    }

    
    
    

}
