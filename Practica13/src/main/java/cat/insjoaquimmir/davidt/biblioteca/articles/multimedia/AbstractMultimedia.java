

package cat.insjoaquimmir.davidt.biblioteca.articles.multimedia;

import cat.insjoaquimmir.davidt.biblioteca.articles.AbstractArticle;
import cat.insjoaquimmir.davidt.biblioteca.articles.Categoria;
import cat.insjoaquimmir.davidt.biblioteca.articles.Promotor;


public abstract class AbstractMultimedia extends AbstractArticle {
    //<editor-fold defaultstate="collapsed" desc="Atributs">
    private String durada;

    
    
//</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Metodes">
    //<editor-fold defaultstate="collapsed" desc="Geters/Setters">
    
    public String getDurada() {
        return durada;
    }

    private void setDurada(String durada) {
        this.durada = durada;
    }
//</editor-fold>
    public AbstractMultimedia(String tit, String aut,Categoria categoria,Promotor promotor,String durada) {
        super(tit, aut,categoria,promotor);
        this.setDurada(durada);
    }
    
    
//</editor-fold>

    @Override
    public String toString() {
        return String.format("Durada: %s \n %s ", this.getDurada(),super.toString()); //To change body of generated methods, choose Tools | Templates.
    }


}
