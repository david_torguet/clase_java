/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.insjoaquimmir.davidt.biblioteca.usuaris;

import cat.insjoaquimmir.davidt.biblioteca.articles.AbstractArticle;
import java.util.ArrayList;
import java.util.Objects;

/**
 *
 * @author tarda
 */
public class UsuariMenorEdat extends AbstractUsuari {
    private UsuariAdult tutor;
    
    public UsuariMenorEdat(String Nom, String cognom1, String cognom2,UsuariAdult tut) {
        super(Nom, cognom1, cognom2, new ArrayList<AbstractArticle>());
        this.setTutor(tut);
    }
//<editor-fold defaultstate="collapsed" desc="GET SET">
    /**
     * @return the tutor
     */
    public UsuariAdult getTutor() {
        return tutor;
    }

    /**
     * @param tutor the tutor to set
     */
    public void setTutor(UsuariAdult tutor) {
        this.tutor = tutor;
    }
//</editor-fold>
    
    @Override
    protected boolean isMenor(){
        return true;
    }

    @Override
    protected int cantidad() {
        return this.getTutor().getTipus().getCuantitat();
    }

    @Override
    public String toString() {
        return String.format("Usuari MenorEdat \n  %s \n Tipus: %s \n Tutor:\n----------\n %s",super.toString(),this.getTutor().getTipus().getDesc(),this.getTutor());
    }

    
    
    
    
    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null) 
            return false;
        if (getClass() != o.getClass()) 
            return false;
        if (!(o instanceof UsuariMenorEdat))
            return false;

        UsuariMenorEdat that = (UsuariMenorEdat) o;
        return Objects.equals(this.tutor.getDNI(), that.tutor.getDNI());
            
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + Objects.hashCode(this.tutor.getDNI());
        return hash;
    }
    
    
    
}
