/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.insjoaquimmir.davidt.biblioteca.articles;

public enum Categoria {
    
    A("Adult"),
    I("Infantil"),
    J("Juvenil");
    private String hola;

    Categoria(String ho){
        this.hola = ho;
    }

    /**
     * @return the hola
     */
    public String getHola() {
        return hola;
    }

    /**
     * @param hola the hola to set
     */
    public void setHola(String hola) {
        this.hola = hola;
    }
    
}
