/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.insjoaquimmir.davidt.biblioteca.usuaris;

public enum UserTipus {
    P("Premium",4),
    N("Normal",2);
    
    private String desc;
    private int cuantitat;
//<editor-fold defaultstate="collapsed" desc="GET SET">
    /**
     * @return the desc
     */
    public String getDesc() {
        return desc;
    }

    /**
     * @param desc the desc to set
     */
    public void setDesc(String desc) {
        this.desc = desc;
    }

    /**
     * @return the cuantitat
     */
    public int getCuantitat() {
        return cuantitat;
    }

    /**
     * @param cuantitat the cuantitat to set
     */
    public void setCuantitat(int cuantitat) {
        this.cuantitat = cuantitat;
    }
//</editor-fold>

    private UserTipus(String desc, int cuantitat) {
        this.setDesc(desc);
        this.setCuantitat(cuantitat);
    }

}
