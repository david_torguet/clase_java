/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.insjoaquimmir.davidt.biblioteca.usuaris;

import cat.insjoaquimmir.davidt.biblioteca.articles.AbstractArticle;
import java.util.ArrayList;

/**
 *
 * @author tarda
 */
public abstract class AbstractUsuari {
    //<editor-fold defaultstate="collapsed" desc="Atributs">
        private String Nom;
        private String cognom1;
        private String cognom2;
        private ArrayList<AbstractArticle> arrayArt;
        
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="GET SET">
        /**
        * @return the Nom
        */
       public String getNom() {
           return Nom;
       }

       /**
        * @param Nom the Nom to set
        */
       public void setNom(String Nom) {
           this.Nom = Nom;
       }

       /**
        * @return the cognom1
        */
       public String getCognom1() {
           return cognom1;
       }

       /**
        * @param cognom1 the cognom1 to set
        */
       public void setCognom1(String cognom1) {
           this.cognom1 = cognom1;
       }

       /**
        * @return the cognom2
        */
       public String getCognom2() {
           return cognom2;
       }

       /**
        * @param cognom2 the cognom2 to set
        */
       public void setCognom2(String cognom2) {
           this.cognom2 = cognom2;
       }
       /**
        * @return the arrayArt
        */
       public ArrayList<AbstractArticle> getArrayArt() {
           return arrayArt;
       }

       /**
        * @param arrayArt the arrayArt to set
        */
       public void setArrayArt(ArrayList<AbstractArticle> arrayArt) {
           this.arrayArt = arrayArt;
       }

    //</editor-fold>

    public AbstractUsuari(String Nom, String cognom1, String cognom2, ArrayList<AbstractArticle> arrayArt) {
        this.setNom(Nom);
        this.setCognom1(cognom1);
        this.setCognom2(cognom2);
        this.setArrayArt(arrayArt);
    }

    public boolean potAgafarArticle(AbstractArticle a){
        boolean pot;
        pot = true;
        if (a.getCat().getHola().equals("Adult") && this.isMenor() == true){
            pot = false;
        }
        if(this.getArrayArt().size() > this.cantidad()){
            pot = false;
        }
        
        return pot;
    }
    
    public void agafaArticle(AbstractArticle a){
        if(this.potAgafarArticle(a)){
            this.getArrayArt().add(a);
        }
    }
    
    public boolean teArticle(AbstractArticle a){
        if(this.getArrayArt().contains(a)){
            return true;
        }else{
            return false;
        }
    }
    
    public void retornaArticle(AbstractArticle a){
        this.getArrayArt().remove(a);
    }
    
    /**
     *
     * @return
     */
    public int sizeArrayArticles(){
        return this.getArrayArt().size();
    }
    
    /**
     *
     * @return
     */
    protected abstract int cantidad();
    protected abstract boolean isMenor();

    @Override
    public String toString() {
        return String.format("Nom: %s \n Primer cognom: %s \n Segon cognom: %s",this.getNom(),this.getCognom1(),this.getCognom2());
    }

    
    
}
