

package cat.insjoaquimmir.davidt.biblioteca.articles.publicacio;

import cat.insjoaquimmir.davidt.biblioteca.articles.Categoria;
import cat.insjoaquimmir.davidt.biblioteca.articles.Promotor;
import java.util.Objects;


public class Llibre extends AbstractPublicacio {
//<editor-fold defaultstate="collapsed" desc="Atributs">
    private String isbn;
    
//</editor-fold>
//<editor-fold defaultstate="collapsed" desc="GET SET">

    /**
     * @return the isbn
     */
    public String getIsbn() {
        return isbn;
    }

    /**
     * @param isbn the isbn to set
     */
    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }
//</editor-fold>

    public Llibre(String titol,String autor,Categoria categoria,Promotor promotor, int num_pag, String algo) {
        super(titol,autor,categoria,promotor,num_pag);
        this.setIsbn(algo);
    }
    public Llibre(String titol,String autor,Categoria categoria, int num_pag, String algo) {
        super(titol,autor,categoria,new Promotor(),num_pag);
        this.setIsbn(algo);
    }

    @Override
    public String toString() {
        return String.format("ISBN: %s \n %s ", this.getIsbn(),super.toString());
    }
        
    
    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null) 
            return false;
        if (getClass() != o.getClass()) 
            return false;
        if (!(o instanceof Llibre))
            return false;

        Llibre that = (Llibre) o;
        return Objects.equals(this.isbn, that.isbn);
            
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 29 * hash + Objects.hashCode(this.isbn);
        return hash;
    }
    
    
    

}
