/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iesjmir.cat.words;

public class WordAnalyzer {
    //<editor-fold defaultstate="collapsed" desc="Atributs">
    private String word;
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Mètodes">

    //<editor-fold defaultstate="collapsed" desc="Constructors">
    
    /**
     * Constructs an analyzer for a given word.
     * 
     * @param aWord
     *            the word to be analyzed
     */
    public WordAnalyzer(String word) {
	this.setWord(word);
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="Getters/Setters">
       /**
     * @return the word
     */
    private String getWord() {
        return word;
    }

    /**
     * @param word the word to set
     */
    private void setWord(String word) {
        this.word = word;
    }
//</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="Mètodes d'objecte">
    
    /**
     * Gets the first repeated character. A character is <i>repeated</i> if it
     * occurs at least twice in adjacent positions. For example, 'l' is repeated
     * in "hollow", but 'o' is not.
     * 
     * @return the first repeated character, or 0 if none found
     */
    public char firstRepeatedCharacter() {
	for (int i = 0; i < getWord().length()-1; i++) {
            char ch = getWord().charAt(i);
            if (ch == getWord().charAt(i + 1))
                return ch;
	}
	return 0;
    }

    /**
     * Gets the first multiply occuring character. A character is
     * <i>multiple</i> if it occurs at least twice in the word, not necessarily
     * in adjacent positions. For example, both 'o' and 'l' are multiple in
     * "hollow", but 'h' is not.
     * 
     * @return the first repeated character, or 0 if none found
     */
    public char firstMultipleCharacter() {
    	for (int i = 0; i < getWord().length(); i++) {
            char ch = getWord().charAt(i);
            if (find(ch, i) >= 0)
            	return ch;
	}
	return 0;
    }

    private int find(char c, int pos) {
    	for (int i = pos+1; i < getWord().length(); i++) {
            if (getWord().charAt(i) == c) {
		return i;
            }
	}
	return -1;
    }

    /**
     * Counts the groups of repeated characters. For example, "mississippi!!!"
     * has four such groups: ss, ss, pp and !!!.
     * 
     * @return the number of repeated character groups
     */
    public int countRepeatedCharacters() {
	int c = 0;
	for (int i = 0; i < getWord().length() - 1; i++) {
            if(i == 0 && getWord().charAt(i) == getWord().charAt(i + 1)){
                c++;
            }else{
                if (getWord().charAt(i) == getWord().charAt(i + 1)) // found a repetition
                {
                if (getWord().charAt(i - 1) != getWord().charAt(i)) // it't the start
                    c++;
		}
            }
	}
	return c;
    }
    //</editor-fold>
    
    //</editor-fold> 

}
