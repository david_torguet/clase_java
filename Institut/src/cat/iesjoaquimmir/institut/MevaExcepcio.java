/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.iesjoaquimmir.institut;

/**
 *
 * @author tarda
 */
public class MevaExcepcio extends Exception {
    private int valor;
    /**
     * Creates a new instance of <code>MevaExcepcio</code> without detail
     * message.
     */
    public MevaExcepcio(int x) {
        valor = x;        
    }
    public String toString(){
        return String.format("Exception MevaExcepcio: Error en la edad, eres menor %d ", valor);
    }

    /**
     * Constructs an instance of <code>MevaExcepcio</code> with the specified
     * detail message.
     *
     * @param msg the detail message.
     */
    public MevaExcepcio(String msg) {
        super(msg);
    }
}
