/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.iesjoaquimmir.institut;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author toni
 */
public class Institut {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws MevaExcepcio, IOException {
        Domicili domicili = new Domicili("Industria", 188,"2n 1a","88000", "Badalona", "Barcelona");
        Alumne alumne = new Alumne("Pedro", "García", "Ramírez","55655678J", 21, domicili);
        Scanner input = new Scanner(System.in);
        
        // veure 02 Primeres classes - exemple 7
        // afegir a apunts
        ArrayList<String> telefons = new ArrayList<String>();
        telefons.add("94343433438");
        telefons.add("94343433435");
        telefons.add("94343433436");
        alumne.setTelefons(telefons);
    
        
        ArrayList<Modul> moduls = new ArrayList<Modul>();
        //De esta manera se cargaran los tres primeros modulos pero el ultimo no, si hacemos que uno de los anteriores de error el resto no se guardaran.
        try{
            moduls.add(new Modul("M01", "Sistemes operatius", 432));
            moduls.add(new Modul("M02", "Base de dades", 150));
            moduls.add(new Modul("M03", "Programacio", 250));
            moduls.add(new Modul("M04", "Llenguatges de Marques", 111));        
        }catch(IllegalArgumentException e){
            System.out.println(e);
        }finally{
            alumne.setModuls(moduls);
        }
        /*
        for(int i=0;i<alumne.getTelefons().size();++i) {
            System.out.println(alumne.getTelefons().get(i));
        }
        */
        /*for(int i=0;i<alumne.getModuls().size();++i) {
            System.out.println(alumne.getModuls().get(i).getNom());
        }	
	//mostra el format cognom1 cognom2, nom
	System.out.println(alumne.getNomComplert());
	
	//mostra si l'alumne és major d'edat
        //He fet aquesta comprovacio a traves de una excepcio propia. MevaExcepcio
	alumne.isMajorEdat();	
	
	//Pinta el mateix que la linia anterior
        System.out.println(domicili.getDomiciliAmigable());
	System.out.println(alumne.getDomicili().getDomiciliAmigable());
	
	System.out.println(alumne.getDomicili().getPis());
	//System.out.println(alumne);
        
        
        */
        /*
        File file = new File("prueba/fichero1.txt");
        if (!file.exists()){
            file.createNewFile();
        }
        */
        /*
        int c;
        try{
           FileReader in = new FileReader(file);
           //FileWriter out = new FileWriter(file);
           while((c = in.read()) != -1){
               System.out.println(c);
           }
           in.close();
        }catch(FileNotFoundException e1){
            System.out.println(e1);
        }catch(IOException e2){
            System.out.println(e2);
        }
        */
        
        
        
        int num;
        do{
            
            System.out.println("0-Crear fichero\n1-Mostrar fichero\n2-Crear persona y añadir al fichero\n3-Copiar el fichero a otro\n4-Salir");
            num = input.nextInt();
            String filename = null;
            switch(num){
                case 0:
                    System.out.println("Dime el nombre del fichero:");
                    filename = input.next();
                    File f = new File("prueba/"+filename+".txt");
                    if(!f.exists()){
                        System.out.println("El fichero no esta crado, lo queres crear? S/N");
                        if(input.next().equalsIgnoreCase("S")){
                            try{
                                f.createNewFile();
                            }catch(IOException e){
                                System.out.println("No se ha podido crear"+e.getMessage());
                            }
                            
                        }
                    }
                    break;
                case 1:
                    System.out.println("Dime el nombre del fichero:");
                    filename = input.next();
                    File f2 = new File("prueba/"+filename+".txt");
                    
                    BufferedReader br = new BufferedReader(new FileReader(f2));
                    String linea = null;
                    while(null != (linea = br.readLine())){
                        System.out.println(linea);                        
                    }
                    br.close();
                    break;
                case 2:
                    System.out.println("Dime el nombre del fichero:");
                    filename = input.next();
                    File f3 = new File("prueba/"+filename+".txt");
                    if(f3.exists()){
                        
                        PrintWriter p_out = new PrintWriter(new FileOutputStream(f3,true));
                        System.out.println("Dime el nombre de la persona: ");
                        String nombre = input.next();
                        System.out.println("Dime el primer apellido de la persona: ");
                        String apellido1 = input.next();
                        System.out.println("Dime el segundo apellido de la persona: ");
                        String apellido2 = input.next();
                        System.out.println("Dime el DNI de la persona: ");
                        String dni = input.next();
                        System.out.println("Dime la edat de la persona: ");
                        int edat = input.nextInt();
                        
                        Alumne alumne1 = new Alumne(nombre, apellido1, apellido2,dni, edat, domicili);
                        
                        System.out.println(alumne1.toString());
                        p_out.append(alumne1.toString()+"\n");                        
                        p_out.close();
                    }else{
                        System.out.println("El fichero no existe, crealo primero");
                    }
                    
                    break;
                case 3:
                    System.out.println("Dime el nombre del fichero de lectura:");
                    filename = input.next();
                    File f4 = new File("prueba/"+filename+".txt");
                    
                    System.out.println("Dime el nombre del fichero de lectura:");
                    String filename1 = input.next();
                    File f5 = new File("prueba/"+filename1+".txt");
                                        
                    BufferedReader br2 = new BufferedReader(new FileReader(f4));
                    String linea2 = null;
                    
                    FileOutputStream fos = new FileOutputStream(f5);
                    BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));
                    
                    while(null != (linea2 = br2.readLine())){
                        bw.append(linea2);
                        
                        
                    }
                    br2.close();
                    
                    
                    
                    
                    
                    
            }       
            
        }while(num != 4);
        
    }
}