/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.iesjoaquimmir.institut;

public class Modul {
    private String nom;
    private String descripcio;
    private int hores;

    //<editor-fold defaultstate="collapsed" desc="constructors">
    
    public Modul(String nom, String descripcio, int hores) {
        setNom(nom);
        setDescripcio(descripcio);
        setHores(hores);
    }

    public Modul(String nom, int hores) {
	this(nom, null, hores);
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="metodes setters/getters">
    /**
     * @return the nom
     */
    public String getNom() {
        return nom;
    }

    /**
     * @param nom the nom to set
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     * @return the descripcio
     */
    public String getDescripcio() {
        return descripcio;
    }

    /**
     * @param descripcio the descripcio to set
     */
    public void setDescripcio(String descripcio) {
        this.descripcio = descripcio;
    }

    /**
     * @return the hores
     */
    public int getHores() {
        return hores;
    }

    /**
     * @param hores the hores to set
     */
    public void setHores(int hores) {
        if(hores < 0){
            throw new IllegalArgumentException(String.format("Error en las horas: %d",hores));
        }else{
            this.hores = hores;        
        }

    }
    //</editor-fold>

}