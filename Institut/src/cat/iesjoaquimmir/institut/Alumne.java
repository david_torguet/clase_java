/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.iesjoaquimmir.institut;

import java.util.ArrayList;


public class Alumne {
    
    //<editor-fold defaultstate="collapsed" desc="atributs">
    
    /**
     * Marca la edat a la que es considera una persona major d'edat
     */
    public static final int MAJOR_EDAT = 18;


    private String nom;
    private String primerCognom;
    private String segonCognom;
    private String dni;
    private ArrayList<String> telefons;
    private ArrayList<Modul> moduls;
    private int edat;
    private Domicili domicili;

    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="mètodes">

    
    //<editor-fold defaultstate="collapsed" desc="constructor">
    public Alumne(String nom, String primerCognom, String segonCognom, String dni, int edat, Domicili domicili) {
        setNom(nom);
        setPrimerCognom(primerCognom); 
        setSegonCognom(segonCognom);
        setDni(dni);
        setEdat(edat);
        setDomicili(domicili);
    }
    
    //</editor-fold>
     
    //<editor-fold defaultstate="collapsed" desc="setters/getters">
    /**
     * @return the nom
     */
    public String getNom() {
        return nom;
    }

    /**
     * @param nom the nom to set
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     * @return the primerCognom
     */
    public String getPrimerCognom() {
        return primerCognom;
    }

    /**
     * @param primerCognom the primerCognom to set
     */
    public void setPrimerCognom(String primerCognom) {
        this.primerCognom = primerCognom;
    }

    /**
     * @return the segonCognom
     */
    public String getSegonCognom() {
        return segonCognom;
    }

    /**
     * @param segonCognom the segonCognom to set
     */
    public void setSegonCognom(String segonCognom) {
        this.segonCognom = segonCognom;
    }

    /**
     * @return the dni
     */
    public String getDni() {
        return dni;
    }

    /**
     * @param dni the dni to set
     */
    public void setDni(String dni) {
        System.out.println(dni.length());
        try{
            
            if(dni.length() != 9){
                throw new IllegalArgumentException("Error en el DNI: "+ dni);
            }else{
                this.dni = dni;
            }
        }catch(IllegalArgumentException e){
            System.out.println(e);
        }
    }

    /**
     * @return the telefons
     */
    public ArrayList<String> getTelefons() {
        return telefons;
    }

    /**
     * @param telefons the telefons to set
     */
    public void setTelefons(ArrayList<String> telefons) {        
        for(int x = 0;x < telefons.size();x++){
            try{
                if(telefons.get(x).length() != 11){
                    throw new IllegalArgumentException(String.format("Error en el telefono: %s", telefons.get(x)));
                }
            }catch(IllegalArgumentException e){
                System.out.println(e);
            }
            
        }
        this.telefons = telefons;
    }

    /**
     * @return the moduls
     */
    public ArrayList<Modul> getModuls() {
        return moduls;
    }

    /**
     * @param moduls the moduls to set
     */
    public void setModuls(ArrayList<Modul> moduls) {
        this.moduls = moduls;
    }

    /**
     * @return the edat
     */
    public int getEdat() {
        return edat;
    }

    /**
     * @param edat the edat to set
     */
    public void setEdat(int edat) {
        this.edat = edat;
    }

    /**
     * @return the domicili
     */
    public Domicili getDomicili() {
        return domicili;
    }

    /**
     * @param domicili the domicili to set
     */
    public void setDomicili(Domicili domicili) {
        this.domicili = domicili;
    }
    
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="mètodes d'objecte">
    public String getNomComplert() {
		return getPrimerCognom() + " " 
                        + getSegonCognom() + ", " + getNom() ;
    }
    
    public void isMajorEdat() throws MevaExcepcio {
            try{
                if(getEdat() < MAJOR_EDAT){
                    throw new MevaExcepcio(getEdat());
                }else{
                    System.out.println(getEdat());
                }
            }catch(MevaExcepcio e){
                System.out.println(e.toString());
            }
    }
    
    //</editor-fold>
   
    
    
    //</editor-fold>

    @Override
    public String toString() {
        return String.format("Nombre: %s , DNI: %s , Edat: %d", this.getNomComplert(),this.getDni(),this.getEdat());
    }
}