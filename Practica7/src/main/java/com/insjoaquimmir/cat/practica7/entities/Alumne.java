/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.insjoaquimmir.cat.practica7.entities;

/**
 *
 * @author tarda
 */
public class Alumne {
    //<editor-fold defaultstate="collapsed" desc="Atributs">
        private String nom;
        private String dni;
        private int edat;
        
        public static final String DNI_DEF = "00000000A";
        public static final String NOM_DEF = "Anonymous";
        public static final int EDAT_DEF = 99;
        
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Metodes">
        //<editor-fold defaultstate="collapsed" desc="GETTER-SETTER">
        public int getEdat() {
            return edat;
        }

        public void setEdat(int edat) {
            this.edat = edat;
        }

        public String getNom() {
            return nom;
        }

        public void setNom(String nom) {
            this.nom = nom;
        }

        public String getDni() {
            return dni;
        }

        public void setDni(String dni) {
            this.dni = dni;
        }
        //</editor-fold>
        public Alumne(String nom,String dni,int edat){
            this.setNom(nom);
            this.setDni(dni);
            this.setEdat(edat);
        }
        public Alumne(String nom,int edat){
            this(nom,DNI_DEF,edat);
        }
        public Alumne(){
            this(NOM_DEF,DNI_DEF,EDAT_DEF);
        }
        
        public String getSalutacio(){
            return String.format("Hola soy %s con dni %s y tengo %d años", this.getNom(),this.getDni(),this.getEdat());
            
        }
    //</editor-fold>


    
    
}
