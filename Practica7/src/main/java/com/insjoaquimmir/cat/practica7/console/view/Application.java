/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.insjoaquimmir.cat.practica7.console.view;

import com.insjoaquimmir.cat.practica7.entities.Alumne;
import java.util.Scanner;


public class Application {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int num = 0;
        do{
            System.out.println("Dime de que dispones: \n1-Nombre-DNI-Edat\n2-Nombre-Edat\n3-Nada\n4-Salir");
            num = input.nextInt();
            
            switch (num) {
                case 1:
                    System.out.println("Dime tu nombre: ");
                    String nombre = input.next();
                    System.out.println("Dime tu DNI: ");
                    String dni = input.next();
                    System.out.println("Dime tu edat: ");
                    int edat = input.nextInt();
                    Alumne al1 = new Alumne(nombre,dni,edat);
                    System.out.println(al1.getSalutacio());
                    break;
                case 2:
                    System.out.println("Dime tu nombre: ");
                    String nom = input.next();
                    System.out.println("Dime tu edat: ");
                    int ed = input.nextInt();
                    Alumne al2 = new Alumne(nom,ed);
                    System.out.println(al2.getSalutacio());
                    break;
                case 3:
                    Alumne al3 = new Alumne();
                    System.out.println(al3.getSalutacio());
                    break;
            }
        }while(num != 4);
    }
}
