package cat.insjoaquimmir.cat.geoapp.model.businesslayer.entities;
/**
 * Plantilla objeto Cuadrado que extiende de la clase Shape
 * @author David Torguet
 */
public class Square extends Shape {
    //<editor-fold defaultstate="collapsed" desc="Atributs">
        private double side;
        

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Metodes">
        /**
         * Constructor amb tots els parametres, envia per super el color foreground i background
         * @param lado Lado del cuadrado
         * @param fore Color foreground del cuadrado
         * @param back Color background del cuadrado
         */
        public Square(double lado, Color fore ,Color back){
            super(fore,back);
            this.setSide(lado);
        }
        /**
         * Constructor con un parametro lado, que llama al constructor con todos los parametros, con foreground i background por defecto.
         * @param lado1 Lado del cuadrado
         */
        public Square(double lado1){
            this(lado1,new Color(Color.MIN_VALUE,Color.MIN_VALUE,Color.MIN_VALUE),new Color(Color.MAX_VALUE,Color.MAX_VALUE,Color.MAX_VALUE));         
        }
        
        /**
         * Constructor sin parametros que llama al constructor con un parametro, el lado por defecto
         */
        public Square(){
            this(1);
        }
        /**
         * Metodo para caclular el Area del cuadrado.
         * @return Devuelve el Area en formato numerico
         */
        public double getArea(){
            return Math.pow(this.getSide(), 2);
        }
        /**
         * Metodo para calcular el Perimetro del cuadrado
         * @return Devuelve el Perimetro en formato numerico
         */
        public double getPerimeter(){
            return this.getSide() * 4;
        }
        
        
        //<editor-fold defaultstate="collapsed" desc="Getters i Setters">
            /**
            * @return the side
            */
           public double getSide() {
               return side;
           }

           /**
            * @param side the side to set
            */
           public void setSide(double side) {
               this.side = side;
           }
            
        //</editor-fold>
 
    //</editor-fold>

   

    

    
}
