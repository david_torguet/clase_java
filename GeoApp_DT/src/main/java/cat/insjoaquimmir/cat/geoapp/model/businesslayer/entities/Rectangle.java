package cat.insjoaquimmir.cat.geoapp.model.businesslayer.entities;

/**
 * Plantilla objeto Rectangulo que extiende de Shape
 * @author David Torguet
 */
public class Rectangle extends Shape { 
    //<editor-fold defaultstate="collapsed" desc="Atributs">
        private double base;
        private double altura;

    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="Metodes">
        /**
         * Constructor cons todos los parametros, llama a super 'Shape' para determinar los colores.
         * @param ba Base del rectangulo.
         * @param alt Altura del rectangulo
         * @param fore Color foreground del rectangulo
         * @param back Color background del rectangulo
         */
        public Rectangle(double ba , double alt, Color fore, Color back){
            super(fore,back);
            this.setBase(ba);
            this.setAltura(alt);
        }
        /**
         * Constructor con dos parametros, llama al constructor con todos los parametros con los colores foreground i background por defecto.
         * @param ba Base del rectangulo.
         * @param alt Altura del rectangulo
         */
        public Rectangle(double ba , double alt){
            this(ba,alt,new Color(Color.MIN_VALUE,Color.MIN_VALUE,Color.MIN_VALUE),new Color(Color.MAX_VALUE,Color.MAX_VALUE,Color.MAX_VALUE));
        }
        /**
         * Constructor vacio que llama al constructor con dos parametros base i altura por defecto a 1.
         */
        public Rectangle(){
            this(1,1);
        }
        /**
         * Calcular el area del rectangulo.
         * @return Area en formato numerico.
         */
        public double getArea(){
            return this.getBase()*this.getAltura();
        }
        /**
         * Calcular el Perimetro del rectangulo.
         * @return Perimetro en formato numerico.
         */
        public double getPerimeter(){
            return 2 * (this.getBase()+this.getAltura());
        }
        
        //<editor-fold defaultstate="collapsed" desc="Getters Setters">
            /**
             * @return the base
             */
            public double getBase() {
                return base;
            }

            /**
             * @param base the base to set
             */
            public void setBase(double base) {
                this.base = base;
            }

            /**
             * @return the altura
             */
            public double getAltura() {
                return altura;
            }

            /**
             * @param altura the altura to set
             */
            public void setAltura(double altura) {
                this.altura = altura;
            }
            
        //</editor-fold>
    //</editor-fold>

    

    

    
    
}
