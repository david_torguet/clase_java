package cat.insjoaquimmir.cat.geoapp.model.businesslayer.entities;

/**
 * Plantilla objeto Esfera que extiende de la clase Shape
 * @author David Torguet
 */
public class Sphere extends Shape {
    //<editor-fold defaultstate="collapsed" desc="Atributs">
        private double radius;

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Metodes">
        /**
         * Constructor amb tots el paramtres, envia foreground i background a super que es Shape.
         * @param rad Radi de la esfera
         * @param fore Foreground color de la esfera
         * @param back Background color de la esfera
         */
        public Sphere(int rad, Color fore, Color back){
            super(fore,back);
            this.setRadius(rad);
             
        }
        /**
         * Constructor amb nomes un paramtre, que crida al constructor amb tots els parametres amb foreground i background per defecte
         * @param rad Radi de la esfera
         */
        public Sphere(int rad){
            this(rad,new Color(Color.MIN_VALUE,Color.MIN_VALUE,Color.MIN_VALUE),new Color(Color.MAX_VALUE,Color.MAX_VALUE,Color.MAX_VALUE));
        }
        /**
         * Creida al constructor amb un paramtre Radi, per defecte a 1.
         */
        public Sphere(){
            this(1);
        }
        /**
         * Metode que calcula el Area de la esfera
         * @return Retorna el Area en format numeric
         */
        public double getArea(){
            return (Math.pow(this.getRadius(),2) * 4 * Math.PI);
        }
        
        /**
         * Metode que calcula e Preimetre de la esfera
         * @return Retorna el Perimetre en format numeric
         */
        public double getPerimeter(){
            return (this.getRadius() * 2 * Math.PI);
        }



        //<editor-fold defaultstate="collapsed" desc="Getters Setters">
           /**
           * @return the radius
           */
          public double getRadius() {
              return radius;
          }

          /**
           * @param radius the radius to set
           */
          public void setRadius(double radius) {
              this.radius = radius;
          }
            
        //</editor-fold>
    //</editor-fold>

  

    


    
}
