package cat.insjoaquimmir.cat.geoapp.model.businesslayer.entities;

/**
 * Plantilla objeto Shape declara los colores de las figuras hijas.
 * @author David Torguet
 */
public class Shape {
    //<editor-fold defaultstate="collapsed" desc="Atributs">
        private Color foregroundColor;
        private Color backgorundColor;
        
        private static int counter = 0;
    //</editor-fold>
        
    //<editor-fold defaultstate="collapsed" desc="Mstodes">
        //<editor-fold defaultstate="collapsed" desc="GEt SET">
            /**
            * @return the foregroundColor
            */
           public Color getForegroundColor() {
               return foregroundColor;
           }

           /**
            * @param foregroundColor the foregroundColor to set
            */
           public void setForegroundColor(Color foregroundColor) {
               this.foregroundColor = foregroundColor;
           }

           /**
            * @return the backgorundColor
            */
           public Color getBackgorundColor() {
               return backgorundColor;
           }

           /**
            * @param backgorundColor the backgorundColor to set
            */
           public void setBackgorundColor(Color backgorundColor) {
               this.backgorundColor = backgorundColor;
           }
           
        //</editor-fold>
        
        /**
         * Constructor amb tots els parametres, Foreground i Background
         * @param fore Color de fora de la figura
         * @param back Color de dins de la figura
         */
        public Shape(Color fore,Color back){
            counter++;
            this.setBackgorundColor(back);
           this.setForegroundColor(fore);
        }
    //</editor-fold>

    

    
}
