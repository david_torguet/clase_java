package cat.insjoaquimmir.cat.geoapp.model.businesslayer.entities;

import java.util.Random;
/**
 * Plantilla objeto Color que extiende de AlphaColor, Declara los colores que tendran las figuras en formato RGB
 * @author David Torguet
 */
public class Color extends AlphaColor {

    //<editor-fold defaultstate="collapsed" desc="Atributs">
        private int red;
        private int green;
        private int blue;

       
        private static int counter = 0;
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Methods">
        //<editor-fold defaultstate="collapsed" desc="Getters Setters">
            /**
            * @return the red
            */
           public int getRed() {
               return red;
           }

           /**
            * @param red the red to set
            */
           public void setRed(int red) {
               this.red = red;
           }

           /**
            * @return the green
            */
           public int getGreen() {
               return green;
           }

           /**
            * @param green the green to set
            */
           public void setGreen(int green) {
               this.green = green;
           }

           /**
            * @return the blue
            */
           public int getBlue() {
               return blue;
           }

           /**
            * @param blue the blue to set
            */
           public void setBlue(int blue) {
               this.blue = blue;
           }

           /**
            * @return the counter
            */
           public static int getCounter() {
               return counter;
           }

        //</editor-fold>
        /**
         * Constructor con todos los parametros.Tiene contador de los colores que se van creando.
         * @param alpha Opacidad del color
         * @param red Parte de rojo
         * @param green Parte de verde
         * @param blue Parte de azul
         */
        public Color(double alpha,int red, int green, int blue){
            super(alpha);
            counter++;
            this.setRed(red);
            this.setGreen(green);
            this.setBlue(blue);
        }
        /**
         * Contructor con parametros de color i llama al contructor con todos los parametros enviando la opacidad por defecto que es 0.
         * @param red Parte de rojo
         * @param green Parte de verde
         * @param blue Parte de azul
         */
        public Color(int red,int green,int blue){
            this(DEF_ALPHA,red,green,blue);            
        }
        /**
         * Constructor que determina la opacidad i la parte de rojo i verde del color, el azul lo pone por defecto 0.
         * @param alpha Opacidad del color
         * @param red Parte de rojo
         * @param green Parte de verde
         */
        public Color(double alpha, int red, int green){
            this(alpha,red,green,Color.MIN_VALUE);
        }
        /**
         * Metodo para a partir de un color en hexadecimal convertirlo a formato RGB.
         * @param color Color en formato Hexadecimal.
         * @return Objeto color con los paramtros rojo, verde i azul extraidos del hexadecimal recivido.
         */
        public static Color fromHexString(String color){
            if(color == null){
                throw new NullPointerException("Es obligatorio");       
            }
            if(!color.matches("^#[0-9A-Fa-f]{6}$")){
                throw new IllegalArgumentException("El texto no esta en el formato correcto");
            }
            return new Color(Integer.parseInt(color.substring(1,3),16),
                             Integer.parseInt(color.substring(3,5),16),
                             Integer.parseInt(color.substring(5,7),16));
        }
        /**
         * Metodo para crear un objeto color con los parametros rojo,verde i azul aleatorios.
         * @return Objeto color.
         */
        public static Color getRandom(){
            Random rand = new Random();
            int rojo = rand.nextInt(MAX_VALUE)+1;
            int verde = rand.nextInt(MAX_VALUE)+1;
            int azul = rand.nextInt(MAX_VALUE)+1;
            
            return new Color(rojo,verde,azul);
        }
        /**
         * Metodo para printar el color en formato RGB, si upper es true se printara en mayusculas sino en minusculas.
         * @param upper Boleano.
         * @return String de los parametros del objeto Color creado.
         */
        public String toRGBString(boolean upper){
            return String.format(upper ? "RGB(%d,%d,%d)  %s" : "rgb(%d,%d,%d) %s", this.getRed(), this.getGreen(), this.getBlue(), super.toRGBString());
        }
        /**
         * 
         * @return LLama al mismo metodo con upper por defecto a false.
         */
        @Override
        public String toRGBString(){
            return toRGBString(false);
        }
        /**
         * Metodo para printar el color en formato Hexadecimal, si upper es true se printara en mayusculas sino en minusculas.
         * @param upper Boleano.
         * @return String de los parametros del objeto Color creado.
         */
        public String toHexString(boolean upper){
            return String.format(upper ? "#%02X%02X%02X %s " : "#%02x%02x%02x %s", this.getRed(), this.getGreen(), this.getBlue(),super.toHexString());
        }
        /**
         * 
         * @return LLama al mismo metodo con upper por defecto a false.
         */
        @Override
        public String toHexString(){
            return toHexString(true);
        }
    //</editor-fold>

    


    
}
