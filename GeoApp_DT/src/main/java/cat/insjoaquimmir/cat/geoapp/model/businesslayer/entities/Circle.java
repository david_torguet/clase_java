package cat.insjoaquimmir.cat.geoapp.model.businesslayer.entities;
/**
 * Plantilla objeto Ciculo que extiende de Shape
 * @author David Torguet
 */
public class Circle extends Shape {
    //<editor-fold defaultstate="collapsed" desc="Atributs">
        private double radius;
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Metodes">
        /**
         * Constructor con todos los parametros, foreground i background los envia a la clase super,Shape.
         * @param rad Radio de la ciscunferencia.
         * @param fore Color foreground de la ciscunferencia.
         * @param back Color background de la ciscunferencia.
         * 
         */
        public Circle(double rad, Color fore ,Color back){
            super(fore,back);                                                                                                                    
            this.setRadius(rad);
        }
        /**
         * Recive solo el radio i llama al constructor con todos los parametros creando los parametros background i foreground por defecto.
         * @param rad Radio de la ciscunferencia.
         * 
         */
        public Circle(int rad){
            this(rad,new Color(Color.MIN_VALUE,Color.MIN_VALUE,Color.MIN_VALUE),new Color(Color.MAX_VALUE,Color.MAX_VALUE,Color.MAX_VALUE));
        }
        /**
         * LLama al constructor con un parametro enviando el radio a 1.
         */
        public Circle(){
            this(1);
        }
        /**
         * @return El araea de la circunferencia ya calculada en formato numerico
         */
        public double getArea(){
            return (Math.PI * Math.pow(this.getRadius(), 2));
        }
        /**
         * 
         * @return El Perimetro de la circunferencia ya calculada en formato numerico
         */
        public double getPerimeter(){
            return (this.getRadius() * 2 * Math.PI);
        }

        //<editor-fold defaultstate="collapsed" desc="Getters Setters">
            /**
            * @return Devuelve el radio de la circunferencia
            */
           public double getRadius() {
               return radius;
           }

           /**
            * @param radius Recive el radio i lo determina an el atributo.
            */
           public void setRadius(double radius) {
               this.radius = radius;
           }
            
        //</editor-fold>
    //</editor-fold>

    

    


    
}
