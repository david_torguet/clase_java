package cat.insjoaquimmir.cat.geoapp.model.businesslayer.entities;
/**
 * Plantilla objeto Alphacolor, Declara la opacidad de los colores que tendran las figuras
 * @author David Torguet
 */
public abstract class AlphaColor {
    private double alpha;
    /**
     * Valor maximo por defecto del Color
     */
    public static final int MAX_VALUE = 255;
    /**
     * Valor minimo por defecto del Color
     */
    public static final int MIN_VALUE = 0;
    /**
     * Nivel de opacidad por defecto 
     */
    public static final double DEF_ALPHA = 0;
    //<editor-fold defaultstate="collapsed" desc="Metodes">
        //<editor-fold defaultstate="collapsed" desc="Get Set">
            /**
            * @return Devuelve el nivel de opacidad en formato numerico.
            */
           public double getAlpha() {
               return alpha;
           }

           /**
            * @param alpha Recive el nivel de opacidad i lo determina en el atributo
            */
           public void setAlpha(double alpha) {
               if(alpha >= 0 && alpha <=1){
                   this.alpha = alpha;
               }
           }
        //</editor-fold>
        /**
         * Constructor que recive la opacidad.
         * @param al Opacidad en formato numerico.
         */
        public AlphaColor(double al){
            this.setAlpha(al);
        }
        /**
         * 
         * @return Devuelve el nivel de opacidad en fomrato String
         */
        public String toRGBString(){
            return String.format("Alpha: %.2f", this.getAlpha());
        }
        /**
         * 
         * @return Devuelve el nivel de opacidad en fomrato Hexadecimal
         */        
        public String toHexString(){
            return String.format("Alpha: %.2f",this.getAlpha());
        }
        
    //</editor-fold>

    
}
