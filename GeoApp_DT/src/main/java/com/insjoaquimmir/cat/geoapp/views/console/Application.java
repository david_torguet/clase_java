package com.insjoaquimmir.cat.geoapp.views.console;

import cat.insjoaquimmir.cat.geoapp.model.businesslayer.entities.Sphere;
import cat.insjoaquimmir.cat.geoapp.model.businesslayer.entities.Circle;
import cat.insjoaquimmir.cat.geoapp.model.businesslayer.entities.Color;
import cat.insjoaquimmir.cat.geoapp.model.businesslayer.entities.Rectangle;
import cat.insjoaquimmir.cat.geoapp.model.businesslayer.entities.Square;
import java.util.Scanner;

/**
 * Pagina para probar todas las clases creads antariormente.
 * @author David Torguet
 */
public class Application {

    /**
     * El Aplication creat es basa en dos menus un per a crear objectes de les clases Cuadrat, Cercle, Rectangle i Esfera, i un segon per a crear Colors.
     * Al primer menu ens preguntara quina de les cuatre figures volem crear, un cop em dit quina figura volem crear 
     * ens preguntara si la volem crear amb parametres o sense, si la volem crear amb paramtres ens preguntara si aquesta figura
     * la volem crear amb colors o sense, si ho fem sense creara els colors per defecte i ens preguntara el/s parametre/s de la
     * figura que volem crear, si decidim crear la nostra figura per defecte es crearan els colors per defectes i el/s 
     * parametre/s de la figura es crearan a 1
     * 
     * Al segon menu serveix per a crear Colors, nomes començar ens preguntara si el volem crear de la manera normal, RGB, llavors 
     * ens demanara que escrivim per consola tant el numero per al color vermell, per al verd i per al blau, la segona opcio es
     * crear el color a partir d'un color en format Hexadecimal, llavors ens demanara que escrivim el color en Hexadecimal
     * i tot seguit es creara el objecte Color amb les seves parts de vermell,verd i blau corrsponents, la ultima opcio que podem escollir
     * es crear un Color de manera aleatoria, llavors es crearan els colors vermell,verd i blau de manera aleatoria i despres
     * creara el objecte.
     *
     * 
     * @param args Scanner para poder tratar los datos puestos por consola.
     *  Son los parametros para el menu que se emplea para crear los objetos.
     *  Rojo utilizado para crear los objetos Color.
     *  Verde utilizado para crear los objetos Color.
     *  Azul utilizado para crear los objetos Color.
     * 
     */
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);  

        int num = 0;
        int num2 = 0;
        int num3 = 0;
        
        int red = 0;
        int green = 0;
        int blue = 0;
        
        
        
        Color alpha1 = new Color(0.80,50,50,50);
        System.out.println(alpha1.toRGBString());
        
        Color alpha2 = new Color(0.60,56,60);
        System.out.println(alpha2.toHexString());
        
        System.out.println("-----------------------------------------------------------------------------");
        do{
            System.out.println("Dame un numero: \n1-Cuadrado\n2-Rectangulo\n3-Circulo\n4-Esfera");
            num = input.nextInt();
            switch(num){
                case 1:
                    System.out.println("1-Cuadrado con parametros\n2-Cuadrado por sdefecto ");
                    num2 = input.nextInt();
                    if(num2 == 1){
                        System.out.println("1-Cuadrado con parametros y con color\n2-Cuadrado Sin color ");
                        num3 = input.nextInt();
                        if(num3 == 1){
                            System.out.println("Dame el lado del cuadrado: ");
                            int lado = input.nextInt();
                            System.out.println("Dame el Rojo Foreground: ");
                             red = input.nextInt();
                            System.out.println("Dame el Verde Foreground: ");
                             green = input.nextInt();
                            System.out.println("Dame el Azul Foreground: ");
                             blue = input.nextInt();
                            Color fore = new Color(red,green,blue);
                            
                            System.out.println("Dame el Rojo Background: ");
                             red = input.nextInt();
                            System.out.println("Dame el Verde Background: ");
                             green = input.nextInt();
                            System.out.println("Dame el Azul Background: ");
                             blue = input.nextInt();
                            Color back = new Color(red,green,blue);
                            
                            Square c1 = new Square(lado,fore,back);
                            System.out.println("Costat: "+c1.getSide());
                            System.out.println("Area: "+c1.getArea());
                            System.out.println("Perimeter: "+c1.getPerimeter());
                            System.out.println("Foreground: "+c1.getForegroundColor().toRGBString());
                            System.out.println("Background: "+c1.getBackgorundColor().toRGBString());
                            break;
                        }else if(num3 == 2){
                            System.out.println("Dame el lado del cuadrado: ");
                            int lado = input.nextInt();
                            Square c1 = new Square(lado);
                            System.out.println("Costat: "+c1.getSide());
                            System.out.println("Area: "+c1.getArea());
                            System.out.println("Perimeter: "+c1.getPerimeter());
                            System.out.println("Foreground: "+c1.getForegroundColor().toRGBString());
                            System.out.println("Background: "+c1.getBackgorundColor().toRGBString());
                            break;
                        }
                            
                    }else if(num2 == 2){
                        Square c1 = new Square();
                        System.out.println("Costat: "+c1.getSide());
                        System.out.println("Area: "+c1.getArea());
                        System.out.println("Perimeter: "+c1.getPerimeter());
                        System.out.println("Foreground: "+c1.getForegroundColor().toRGBString());
                        System.out.println("Background: "+c1.getBackgorundColor().toRGBString());
                        break;
                    }
                    
                case 2:
                    System.out.println("1-Rectangulo con parametros\nRectangulo por sdefecto ");
                    num2 = input.nextInt();
                    if(num2 == 1){
                        System.out.println("1-Rectangulo con parametros y con color\n2-Rectangulo Sin color ");
                        num3 = input.nextInt();
                        if(num3 == 1){
                            System.out.println("Dame la base del rectangulo: ");
                            int base = input.nextInt();
                            System.out.println("Dame la altura del rectangulo: ");
                            int altura = input.nextInt();
                            System.out.println("Dame el Rojo Foreground: ");
                             red = input.nextInt();
                            System.out.println("Dame el Verde Foreground: ");
                             green = input.nextInt();
                            System.out.println("Dame el Azul Foreground: ");
                             blue = input.nextInt();
                            Color fore = new Color(red,green,blue);
                            
                            System.out.println("Dame el Rojo Background: ");
                             red = input.nextInt();
                            System.out.println("Dame el Verde Background: ");
                             green = input.nextInt();
                            System.out.println("Dame el Azul Background: ");
                             blue = input.nextInt();
                            Color back = new Color(red,green,blue);
                            
                            Rectangle r1 = new Rectangle(base,altura,fore,back);
                            System.out.println("Base: "+r1.getBase());
                            System.out.println("Altura: "+r1.getAltura());
                            System.out.println("Area: "+r1.getArea());
                            System.out.println("Perimeter: "+r1.getPerimeter());
                            System.out.println("Foreground: "+r1.getForegroundColor().toRGBString());
                            System.out.println("Background: "+r1.getBackgorundColor().toRGBString());
                            break;
                        }else if(num3 == 2){
                            System.out.println("Dame la base del rectangulo: ");
                            int base = input.nextInt();
                            System.out.println("Dame la altura del rectangulo: ");
                            int altura = input.nextInt();
                            Rectangle r1 = new Rectangle(base,altura);
                            System.out.println("Base: "+r1.getBase());
                            System.out.println("Altura: "+r1.getAltura());
                            System.out.println("Area: "+r1.getArea());
                            System.out.println("Perimeter: "+r1.getPerimeter());
                            System.out.println("Foreground: "+r1.getForegroundColor().toRGBString());
                            System.out.println("Background: "+r1.getBackgorundColor().toRGBString());
                            break;
                        }

                    }else if(num2 == 2){
                        Rectangle r1 = new Rectangle();
                        System.out.println("Base: "+r1.getBase());
                        System.out.println("Altura: "+r1.getAltura());
                        System.out.println("Area: "+r1.getArea());
                        System.out.println("Perimeter: "+r1.getPerimeter());
                        System.out.println("Foreground: "+r1.getForegroundColor().toRGBString());
                        System.out.println("Background: "+r1.getBackgorundColor().toRGBString());
                        break;
                    }
                case 3:
                    System.out.println("1-Circunferencia con parametros\nCircunferencia por sdefecto ");
                    num2 = input.nextInt();
                    if(num2 == 1){
                        System.out.println("1-Circunferencia con parametros y con color\n2-Circunferencia Sin color ");
                        num3 = input.nextInt();
                        if(num3 == 1){
                            System.out.println("Dame el radio de la circunferencia:: ");
                            int rad = input.nextInt();
                            System.out.println("Dame el Rojo Foreground: ");
                             red = input.nextInt();
                            System.out.println("Dame el Verde Foreground: ");
                             green = input.nextInt();
                            System.out.println("Dame el Azul Foreground: ");
                             blue = input.nextInt();
                            Color fore = new Color(red,green,blue);
                            
                            System.out.println("Dame el Rojo Background: ");
                             red = input.nextInt();
                            System.out.println("Dame el Verde Background: ");
                             green = input.nextInt();
                            System.out.println("Dame el Azul Background: ");
                             blue = input.nextInt();
                            Color back = new Color(red,green,blue);
                            
                            Circle cir1 = new Circle(rad,fore,back);
                            System.out.println("Circulo: ");
                            System.out.println("Radio: "+cir1.getRadius());        
                            System.out.println("Area: "+cir1.getArea());
                            System.out.println("Perimeter: "+cir1.getPerimeter());
                            System.out.println("Foreground: "+cir1.getForegroundColor().toRGBString());
                            System.out.println("Background: "+cir1.getBackgorundColor().toRGBString());
                            break;
                        }else if(num3 == 2){
                            System.out.println("Dame el radio de la circunferencia:: ");
                            int rad = input.nextInt();
                            Circle cir1 = new Circle(rad);
                            System.out.println("Circulo: ");
                            System.out.println("Radio: "+cir1.getRadius());        
                            System.out.println("Area: "+cir1.getArea());
                            System.out.println("Perimeter: "+cir1.getPerimeter());
                            System.out.println("Foreground: "+cir1.getForegroundColor().toRGBString());
                            System.out.println("Background: "+cir1.getBackgorundColor().toRGBString());
                        }                        
                    }else if(num2 == 2){
                        Circle cir1 = new Circle();
                        System.out.println("Circulo: ");
                        System.out.println("Radio: "+cir1.getRadius());        
                        System.out.println("Area: "+cir1.getArea());
                        System.out.println("Perimeter: "+cir1.getPerimeter());
                        break;
                    }
                    
                case 4:
                    System.out.println("1-Esfera con parametros\nEsfera por sdefecto ");
                    num2 = input.nextInt();
                    if(num2 == 1){
                        System.out.println("1-Esfera con parametros y con color\n2-Esfera Sin color ");
                        num3 = input.nextInt();
                        if(num3 == 1){
                            System.out.println("Dame el radio de la esfera: ");
                            int radius = input.nextInt();
                            System.out.println("Dame el Rojo Foreground: ");
                             red = input.nextInt();
                            System.out.println("Dame el Verde Foreground: ");
                             green = input.nextInt();
                            System.out.println("Dame el Azul Foreground: ");
                             blue = input.nextInt();
                            Color fore = new Color(red,green,blue);
                            
                            System.out.println("Dame el Rojo Background: ");
                             red = input.nextInt();
                            System.out.println("Dame el Verde Background: ");
                             green = input.nextInt();
                            System.out.println("Dame el Azul Background: ");
                             blue = input.nextInt();
                            Color back = new Color(red,green,blue);
                            
                            Sphere s1 = new Sphere(radius,fore,back);
                            System.out.println("Esfera: ");
                            System.out.println("Radio: "+s1.getRadius());        
                            System.out.println("Area: "+s1.getArea());
                            System.out.println("Perimeter: "+s1.getPerimeter());
                            System.out.println("Foreground: "+s1.getForegroundColor().toRGBString());
                            System.out.println("Background: "+s1.getBackgorundColor().toRGBString());
                            break;
                        }else if(num3 == 2){
                            System.out.println("Dame el radio de la esfera: ");
                            int radius = input.nextInt();
                            Sphere s1 = new Sphere(radius);
                            System.out.println("Esfera: ");
                            System.out.println("Radio: "+s1.getRadius());        
                            System.out.println("Area: "+s1.getArea());
                            System.out.println("Perimeter: "+s1.getPerimeter());
                            System.out.println("Foreground: "+s1.getForegroundColor().toRGBString());
                            System.out.println("Background: "+s1.getBackgorundColor().toRGBString());
                        }
                        
                    }else if(num2 == 2){
                        Sphere s1 = new Sphere();
                        System.out.println("Esfera: ");
                        System.out.println("Radio: "+s1.getRadius());        
                        System.out.println("Area: "+s1.getArea());
                        System.out.println("Perimeter: "+s1.getPerimeter());
                        System.out.println("Foreground: "+s1.getForegroundColor().toRGBString());
                        System.out.println("Background: "+s1.getBackgorundColor().toRGBString());
                        break;
                    }
            }
                    
        }while(num != 5);
        

        num = 0;
        do{
            System.out.println("Dame un numero: \n1-Crear Color\n2-Crear Color con Hexadecimal\n3-Crear Color Aleatorio");
            num = input.nextInt();
            switch(num){
                case 1:
                    System.out.println("Dame el Rojo: ");
                     red = input.nextInt();
                    System.out.println("Dame el Verde: ");
                     green = input.nextInt();
                    System.out.println("Dame el Azul: ");
                     blue = input.nextInt();
                    
                    Color cl1 = new Color(red,green,blue);
                    System.out.println("To Hexadecimal: "+cl1.toHexString(true));
                    System.out.println("To RGB: "+ cl1.toRGBString(true));

                    break;
                
                case 2:
                    System.out.println("Dame el Hexadecimal: ");
                    String hex = input.next();
                    Color cl2 = Color.fromHexString(hex);
                    System.out.println("To Hexadecimal: "+cl2.toHexString(true));
                    System.out.println("To RGB: "+ cl2.toRGBString(true));
                    break;
                    
                case 3:
                    Color cl3 = Color.getRandom();
                    System.out.println("To Hexadecimal: "+cl3.toHexString(true));
                    System.out.println("To RGB: "+ cl3.toRGBString(true));
                    break;
                    
            }
                    
        }while(num != 4);
        
        
        
        
    }
}
 